#!/usr/bin/sh
# SPDX-License-Identifier: GPL-3.0-only
# (C) Copyright 2019-2022 Stephane Neveu
# (C) Copyright 2019-2022 Nicolas Bouchinet

set -o errexit -o nounset

disable_systemd(){
	systemctl stop leguichet.service || true
	systemctl disable leguichet.service || true
	systemctl disable leguichet-in.service || true
	systemctl disable leguichet-transit.service || true
	systemctl disable leguichet-out.service || true
	systemctl daemon-reload
}

main() {
	disable_systemd

	files="
		/var/log/leguichet-in
		/var/log/leguichet-transit
		/var/log/leguichet-out
		/etc/systemd/system/leguichet.service
		/etc/systemd/system/leguichet-in.service
		/etc/systemd/system/leguichet-transit.service
		/etc/systemd/system/leguichet-out.service
		/etc/leguichet/leguichet-in.conf
		/etc/leguichet/leguichet-transit.conf
		/etc/leguichet/leguichet-out.conf
		/usr/bin/leguichet-in
		/usr/bin/leguichet-transit
		/usr/bin/leguichet-out
		/etc/apparmor.d/usr.bin.leguichet-in
		/etc/apparmor.d/usr.bin.leguichet-transit
		/etc/apparmor.d/usr.bin.leguichet-out
		/etc/apparmor.d/local/usr.sbin.clamd
	"

	dirs="
		/etc/systemd/system/leguichet-in.service.d/
		/etc/systemd/system/leguichet-transit.service.d/
		/etc/systemd/system/leguichet-out.service.d/
		/var/local/in
		/var/local/transit
		/var/local/out
		/run/diode-in
		/run/diode-out
		/usr/share/leguichet
		/etc/leguichet
	"

	users="
		leguichet-out
		leguichet-transit
		leguichet-in
	"

	readonly files dirs users

	for file in ${files}; do
		[ -f "${file:-}" ] && rm -rf "${file}"
	done
	for dir in ${dirs}; do
		[ -d "${dir:-}" ] && rm -rf "${dir}"
	done
	for user in ${users}; do
		if getent passwd "${user}" >/dev/null ; then
			userdel "${user}" >/dev/null
		fi
		if getent group "${user}" >/dev/null ; then
			groupdel "${user}" >/dev/null
		fi
	done

	echo "Le-Guichet should be removed !"
}

main "$@"

