#!/usr/bin/sh
# SPDX-License-Identifier: GPL-3.0-only
# (C) Copyright 2019-2022 Stephane Neveu
# (C) Copyright 2019-2022 Nicolas Bouchinet

set -o errexit -o nounset

# Install /var/log/ directories.
create_varlog(){
	if [ ! -d "/var/log/leguichet-usb" ]; then
		install -d -m 0755 -o root -g root /var/log/leguichet-usb
	fi
}

# Install bash scripts in /usr/bin/.
install_scripts(){
	if [ -d "/usr/bin" ]; then
		if [ -f "usb/leguichet-usb-in" ]; then
			install -v -o root -g root -m 0700 usb/leguichet-usb-in /usr/bin/
		else
			echo "Script usb-in cannot be found !"
		fi
		if [ -f "usb/leguichet-usb-out" ]; then
			install -v -o root -g root -m 0700 usb/leguichet-usb-out /usr/bin/
		else
			echo "Script usb-out cannot be found !"
		fi
	fi
}
# Install systemd units
install_units(){
	if [ -d "/etc/systemd/system" ]; then
		if [ -f "debian/leguichet-usb-in.service" ]; then
			install -v -o root -g root -m 0700 debian/leguichet-usb-in.service /etc/systemd/system/leguichet-usb-in@.service
		else
			echo "Unit leguichet-usb-in.Service cannot be found !"
		fi
		if [ -f "debian/leguichet-usb-out.service" ]; then
			install -v -o root -g root -m 0700 debian/leguichet-usb-out.service /etc/systemd/system/leguichet-usb-out@.service
		else
			echo "Unit leguichet-usb-out.service cannot be found !"
		fi
	fi
}

# Call the above functions to perform the installation.
create_varlog
install_scripts
install_scripts
install_units

# Now let's try to be more verbose for the end users
cat <<-EOF
	  _                  _____       _      _          _
	 | |                / ____|     (_)    | |        | |
	 | |     ___ ______| |  __ _   _ _  ___| |__   ___| |_
	 | |    / _ \______| | |_ | | | | |/ __| '_ \ / _ \ __|
	 | |___|  __/      | |__| | |_| | | (__| | | |  __/ |
	 |______\___|       \_____|\__,_|_|\___|_| |_|\___|\__|
	
EOF
echo "USB Installation completed !"


cat <<-EOF
	-|>- Let's start now creating a new udev rule to match your USB devices.
	     - Open a new terminal and type the following:
	       udevadm monitor --property | grep ID_SERIAL_SHORT
	     - Then, plug your first USB device in and note the short serial number.
	     - When done, create a new file in /etc/udev/rules.d/90-usb-leguichet.rules and add the following line:
	       ACTION==\"add\", KERNEL==\"sdb[a-z][0-9]\", SUBSYSTEM==\"block\", ENV{ID_SERIAL_SHORT}==\"XXXXX\", RUN+=\"systemctl start leguichet-usb-in@%k.service\"
	     - Update the ID_SERIAL_SHORT variable according to the previously noted value.
	     - Repete now this operation for the second USB device.
	     - And add now the following line in /etc/udev/rules.d/90-usb-leguichet.rules
	       ACTION==\"add\", KERNEL==\"sdb[a-z][0-9]\", SUBSYSTEM==\"block\", ENV{ID_SERIAL_SHORT}==\"YYYYY\", RUN+=\"systemctl start leguichet-usb-out@%k.service\"
	     - Update the ID_SERIAL_SHORT variable according to your needs.
	     - Reload the udev rules:
	       udevadm control --reload-rules
	     - That's it !
EOF
