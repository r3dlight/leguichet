#!/usr/bin/sh
# SPDX-License-Identifier: GPL-3.0-only
# (C) Copyright 2019-2022 Stephane Neveu
# (C) Copyright 2019-2022 Nicolas Bouchinet

set -o errexit -o nounset

dirs="
	/var/log/leguichet-usb
	/usr/share/leguichet/sound
"

files="
	/usr/bin/leguichet-usb-in
	/usr/bin/leguichet-usb-out
	/etc/systemd/system/leguichet-usb-in.service
	/etc/systemd/system/leguichet-usb-out.service
	/etc/systemd/system/leguichet-usb-in.service
	/etc/systemd/system/leguichet-usb-out.service
	/etc/udev/rules.d/90-usb-leguichet.rules
"

for file in ${files}; do
	if [ -f "${file:-}" ]; then
		rm "${file}"
	fi
done

for dir in ${dirs}; do
	if [ -d "${dir:-}" ]; then
		rm -rf "${dir}"
	fi
done

echo "Le-Guichet USB should be removed !"
