#!/usr/bin/sh
# SPDX-License-Identifier: GPL-3.0-only
# (C) Copyright 2019-2022 Stephane Neveu
# (C) Copyright 2019-2022 Nicolas Bouchinet

set -o errexit -o nounset

HOME_GUICHET_IN="/var/local/in"
readonly HOME_GUICHET_IN

HOME_GUICHET_TRANSIT="/var/local/transit"
readonly HOME_GUICHET_IN

HOME_GUICHET_OUT="/var/local/out"
readonly HOME_GUICHET_OUT

U_GUICHET_IN="leguichet-in"
readonly U_GUICHET_IN

U_GUICHET_OUT="leguichet-out"
readonly U_GUICHET_OUT

U_GUICHET_TRANSIT="leguichet-transit"
readonly U_GUICHET_TRANSIT

# Create leguichet-in, leguichet-transit and leguichet-out users if necessary.
add_users() {
	if ! getent passwd $U_GUICHET_IN >/dev/null ; then
		useradd -r -M --shell /bin/false -d $HOME_GUICHET_IN $U_GUICHET_IN
		install -d -m 0750 -o $U_GUICHET_IN -g $U_GUICHET_IN $HOME_GUICHET_IN
	fi
	if ! getent passwd $U_GUICHET_TRANSIT >/dev/null ; then
		useradd -r -M --shell /bin/false -d $HOME_GUICHET_TRANSIT -G $U_GUICHET_IN $U_GUICHET_TRANSIT
		install -d -m 0750 -o $U_GUICHET_TRANSIT -g $U_GUICHET_TRANSIT $HOME_GUICHET_TRANSIT
	fi
	if ! getent passwd $U_GUICHET_OUT >/dev/null ; then
		useradd -r -M --shell /bin/false -d $HOME_GUICHET_OUT -G $U_GUICHET_TRANSIT $U_GUICHET_OUT
		install -d -m 0750 -o $U_GUICHET_OUT -g $U_GUICHET_OUT $HOME_GUICHET_OUT
	fi
}

# Install /var/log/ directories.
create_varlog() {
	if [ ! -d "/var/log/leguichet-in" ]; then
		install -d -m 0750 -o $U_GUICHET_IN -g $U_GUICHET_IN /var/log/leguichet-in
	fi
	if [ ! -d "/var/log/leguichet-out" ]; then
		install -d -m 0750 -o $U_GUICHET_OUT -g $U_GUICHET_OUT /var/log/leguichet-out
	fi
	if [ ! -d "/var/log/leguichet-transit" ]; then
		install -d -m 0750 -o $U_GUICHET_TRANSIT -g $U_GUICHET_TRANSIT /var/log/leguichet-transit
	fi
}

# Install ELF binaries in /usr/bin/.
install_bin() {
	if [ -d "/usr/bin" ]; then
		if [ -f "bin/leguichet-in" ]; then
			install -v -o $U_GUICHET_IN -g $U_GUICHET_IN -m 0500 bin/leguichet-in /usr/bin/
		else
			echo "Binary ./bin/leguichet-in cannot be found !"
		fi
	fi
	if [ -d "/usr/bin" ]; then
		if [ -f "bin/leguichet-transit" ]; then
			install -v -o $U_GUICHET_TRANSIT -g $U_GUICHET_TRANSIT -m 0500 bin/leguichet-transit /usr/bin/
		else
			echo "Binary ./bin/leguichet-transit cannot be found !"
		fi
	fi
	if [ -d "/usr/bin" ]; then
		if [ -f "bin/leguichet-out" ]; then
			install -v -o $U_GUICHET_OUT -g $U_GUICHET_OUT -m 0500 bin/leguichet-out /usr/bin/
		else
			echo "Binary ./bin/leguichet-out cannot be found !"
		fi
	fi
}

# Install systemd units.
install_systemd_units(){
	if [ -d "/etc/systemd/system/" ]; then
		install -v -o root -g root -m 0644 debian/leguichet.service /etc/systemd/system/leguichet.service
		install -v -o root -g root -m 0644 debian/leguichet-in.service /etc/systemd/system/leguichet-in.service
		install -v -o root -g root -m 0644 debian/leguichet-transit.service /etc/systemd/system/leguichet-transit.service
		install -v -o root -g root -m 0644 debian/leguichet-out.service /etc/systemd/system/leguichet-out.service
		if [ ! -d "/etc/systemd/system/leguichet-in.service.d/" ]; then
			install -d -m 0750 -o root -g root /etc/systemd/system/leguichet-in.service.d/
			if [ -f debian/leguichet-in.security ]; then
				install -v -o root -g root -m 0644 debian/leguichet-in.security /etc/systemd/system/leguichet-in.service.d/security.conf
			fi
		fi
		if [ ! -d "/etc/systemd/system/leguichet-transit.service.d/" ]; then
			install -d -m 0750 -o root -g root /etc/systemd/system/leguichet-transit.service.d/
			if [ -f debian/leguichet-transit.security ]; then
				install -v -o root -g root -m 0644 debian/leguichet-transit.security /etc/systemd/system/leguichet-transit.service.d/security.conf
			fi
		fi
		if [ ! -d "/etc/systemd/system/leguichet-out.service.d/" ]; then
			install -d -m 0750 -o root -g root /etc/systemd/system/leguichet-out.service.d/
			if [ -f debian/leguichet-out.security ]; then
				install -v -o root -g root -m 0644 debian/leguichet-out.security /etc/systemd/system/leguichet-out.service.d/security.conf
			fi
		fi
	else
		echo "Path /etc/systemd/system/ not found, this system isprobably not using systemd !"
	fi
}

# Install le guichet config files.
install_config() {
	if [ ! -d "/etc/leguichet" ]; then
		echo "Creating /etc/leguichet directory."
		install -d -m 0755 -o root -g root /etc/leguichet
	fi
	if [ -d "/etc/leguichet/" ]; then
		echo "Installing configuration files for Le-Guichet."
		install -v -o $U_GUICHET_IN -g $U_GUICHET_IN -m 0600 debian/leguichet-in.default /etc/leguichet/leguichet-in.conf
		install -v -o $U_GUICHET_TRANSIT -g $U_GUICHET_TRANSIT -m 0600 debian/leguichet-transit.default /etc/leguichet/leguichet-transit.conf
		install -v -o $U_GUICHET_OUT -g $U_GUICHET_OUT -m 0600 debian/leguichet-out.default /etc/leguichet/leguichet-out.conf
	fi
}

# Install apparmor profiles.
install_apparmor_profiles() {
	if [ -d "/etc/apparmor.d/" ]; then
		echo "Installing Apparmor policies:"
		install -v -o root -g root -m 0644 debian/usr.bin.leguichet-in /etc/apparmor.d/usr.bin.leguichet-in
		install -v -o root -g root -m 0644 debian/usr.bin.leguichet-transit /etc/apparmor.d/usr.bin.leguichet-transit
		install -v -o root -g root -m 0644 debian/usr.bin.leguichet-out /etc/apparmor.d/usr.bin.leguichet-out
		install -v -o root -g root -m 0644 debian/usr.sbin.clamd /etc/apparmor.d/local/usr.sbin.clamd
	else
		echo "WARNING: Directory /etc/apparmor.d/ not found ! Cannot install Apparmod policies..."
	fi
	if [ -x "/usr/sbin/apparmor_parser" ]; then
		echo "Applying Apparmor policies !"
		/usr/sbin/apparmor_parser -r /etc/apparmor.d/usr.bin.leguichet-in
		/usr/sbin/apparmor_parser -r /etc/apparmor.d/usr.bin.leguichet-transit
		/usr/sbin/apparmor_parser -r /etc/apparmor.d/usr.bin.leguichet-out
		/usr/sbin/apparmor_parser -r /etc/apparmor.d/usr.sbin.clamd
	else
		echo "WARNING: Cannot find /usr/sbin/apparmor_parser !"
		echo "WARNING: Will not apply new Apparmor policies for leguichet !"
	fi
}

# Set ACLs on directories.
set_acls(){
	if [ -d "$HOME_GUICHET_IN" ]; then
		setfacl -b $HOME_GUICHET_IN
		setfacl -m u:clamav:rx,g:$U_GUICHET_IN:rwx $HOME_GUICHET_IN
	fi
	if [ -d "$HOME_GUICHET_TRANSIT" ]; then
		setfacl -b $HOME_GUICHET_TRANSIT
		setfacl -m g:$U_GUICHET_TRANSIT:rwx $HOME_GUICHET_TRANSIT
	fi
	if [ -d "$HOME_GUICHET_OUT" ]; then
		setfacl -b $HOME_GUICHET_OUT
		setfacl -m g:$U_GUICHET_OUT:rwx $HOME_GUICHET_OUT
	fi
}

# Install a simple "demo" Yara rule.
install_yara_rule(){
	if [ ! -d "/usr/share/leguichet/" ]; then
		echo "Installing a minimal YARA index.yar in /usr/share/leguichet/"
		install -d -m 0755 -o root -g root /usr/share/leguichet/
		install -d -m 0755 -o root -g root /usr/share/leguichet/rules
		install -v -m 0644 -o root -g root  yara/index.yar /usr/share/leguichet/rules/index.yar
	fi
}

# Enable systemd units.
enable_systemd() {
	echo "Enabling Le-Guichet system in systemd..."
	systemctl enable leguichet-in.service
	systemctl enable leguichet-out.service
	systemctl enable leguichet-transit.service
	systemctl enable leguichet.service --now
}

main() {
	# Call the above functions to perform the installation.
	add_users
	create_varlog
	install_bin
	install_systemd_units
	install_config
	install_apparmor_profiles
	set_acls
	install_yara_rule
	enable_systemd

	# Now let's try to be more verbose for the end users
	cat <<-EOF
	  _                  _____       _      _          _
	 | |                / ____|     (_)    | |        | |
	 | |     ___ ______| |  __ _   _ _  ___| |__   ___| |
	 | |    / _ \______| | |_ | | | | |/ __| '_ \ / _ \ __|
	 | |___|  __/      | |__| | |_| | | (__| | | |  __/ |
	 |______\___|       \_____|\__,_|_|\___|_| |_|\___|\__|
	
	EOF
	echo "Installation completed !"

	INGID=$(getent group $U_GUICHET_IN | awk -F: '{printf "%d\n", $3}')
	readonly INGID

	OUTGID=$(getent group $U_GUICHET_OUT | awk -F: '{printf "%d\n", $3}')
	readonly OUTGID

	cat <<-EOF
		-|>- You can now create new users belonging to group leguichet-in
		     to be able to deposit files into /var/local/in/
		     sudo adduser  --home /var/local/in --gid $INGID untrusted_user
		-|>- You also need to create new users belonging to group leguichet-out
		     to be able to retrieve files from /var/local/out/
		     sudo adduser  --home /var/local/out --gid $OUTGID trusted_user
	EOF
}

main "$@"
 
