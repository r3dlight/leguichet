[![Made with Rust](https://img.shields.io/badge/Made%20with-Rust-9cf)](https://www.rust-lang.org/)
[![Build Status](https://gitlab.com/r3dlight/leguichet/badges/master/pipeline.svg)](https://gitlab.com/r3dlight/leguichet/commits/master)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![codecov](https://codecov.io/gl/r3dlight/leguichet/branch/master/graph/badge.svg)](https://codecov.io/gl/r3dlight/leguichet)

<div  align="center">

<img  src ="img/logo.png"  alt="LeGuichet RER"  width=300px/>

</div>

  

# What is it ?

  
**Le-Guichet** is a modern decontamination station prototype, fast, multithreaded which aims to be secure.

Le-guichet provides an easy solution to verify your documents. It can be used as a simple “decontamination station” or act as a gateway to transfert untrusted files to higher security level networks. 
You can manage and create your own Yara rules and add them easily to Le-Guichet in order to inspect your files with more efficiency.

## User documentation

User documentation can be found here : [https://le-guichet.com](https://le-guichet.com)


## Gateway

<div  align="center">
<img  src ="img/flowchart.png"  alt="LeGuichet gateway"  width=900px/>
</div>

## USB mode

<div  align="center">
<img  src ="img/usbmode.jpg"  alt="LeGuichet USB"  width=900px/>
</div>



## Security
  

- Memory-safe
- Thread-safe
- Mode 2 seccomp (x86_64 and aarch64 supported)
- Tested with cargo audit & Clippy
- Daemons are all running with unprivileged users
- Apparmor policies
- Advanced Systemd protections per daemon tuned with **systemd-analyse security**:
<div  align="center">
<img  src ="img/systemd-security.png"  alt="systemd protections"  width=600px/>
</div>


**Note:** LeGuichet is tested with Grsecurity kernel patch. It just requires MPROTECT to be disabled on ELF binaries.

## Demo Video
<div  align="center">
<a  href="https://www.youtube.com/watch?feature=player_embedded&v=0aNTVHz886c"  target="_blank"><img  src="https://img.youtube.com/vi/0aNTVHz886c/0.jpg"  alt="Le-Guichet demo video"  width="360"  height="300"  border="20"  /></a>
<a  href="https://www.youtube.com/watch?feature=player_embedded&v=efFI8J4SG7U"  target="_blank"><img  src="https://img.youtube.com/vi/efFI8J4SG7U/0.jpg"  alt="Le-Guichet USB demo video"  width="360"  height="300"  border="20"  /></a>
</div>

# Installation

For the installation procedure, please follow the [online documentation](https://le-guichet.com/installation.html).

## To do
  
- Debian packaging via Cargo
- Secomp strict 
