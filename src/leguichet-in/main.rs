// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "guichet-in".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains the main function.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

#[macro_use]
extern crate log;
use crate::errors::*;
use clap::{arg, crate_version, Command};
use flexi_logger::{opt_format, Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
use std::net::IpAddr;
use std::path::Path;
use std::thread as main_thread;
use std::time::Duration;

mod errors;
mod leguichet;
mod sandbox;

// Defines configuration and loop over inotify events.
#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    // Setting up allowed syscalls.
    #[cfg(target_os = "linux")]
    sandbox::init().context("Cannot apply first seccomp mode 2 syscall filter.")?;

    // Start clap CLI definition
    let matches = Command::new("LeGuichet-in")
        .version(crate_version!())
        .author("Stephane N.")
        .about("LeGuichet-in, input window.")
        .arg(
            arg!( -g --guichetin <PATH> "Sets le-guichet's path for incoming files")
                .default_value("/var/local/in/"),
        )
        .arg(
            arg!( -d --diodein <PATH> "Sets the diode-in folder path for transfering files")
                .default_value("/run/diode-in/"),
        )
        .arg(
            arg!( -l --log <PATH> "Sets a custom log file path")
                .default_value("/var/log/leguichet-in/"),
        )
        .arg(arg!( -c --clamavip <IP> "Clamav IP addr").default_value("127.0.0.1"))
        .arg(
            arg!( -p --clamavport <PORT> "Clamav port number")
                .default_value("3310")
                .validator(|s| s.parse::<u16>()),
        )
        .arg(
            arg!( -s --rotate_over_size <SIZE> "Rotate logs over specified size (bytes)")
                .default_value("3000000")
                .validator(|s| s.parse::<u64>()),
        )
        .arg(
            arg!( -n --number_of_log_files <NUMBER> "Number of log files to keep")
                .default_value("3")
                .validator(|s| s.parse::<usize>()),
        )
        .arg(
            arg!( -m --maxfilesize <NUMBER> "Maximum file size (in bytes) allowed for transfering")
                .default_value("500000000")
                .validator(|s| s.parse::<u64>()),
        )
        .get_matches();

    // Get values or use default ones.
    let guichetin: &str = matches.value_of("guichetin").unwrap_or("/var/local/in/");
    let diode: &str = matches.value_of("diode-in").unwrap_or("/run/diode-in/");
    let log_path: &str = matches.value_of("log").unwrap_or("/var/log/leguichet-in/");
    let clamav_ip: &str = matches.value_of("clamavip").unwrap_or("127.0.0.1");
    let clamav_port: &str = matches.value_of("clamavport").unwrap_or("3310");
    let clamav_port_int = clamav_port
        .parse::<u16>()
        .context("Cannot convert clamav port string into u16 integer !")?;
    let rotate_over_size: &str = matches.value_of("rotate_over_size").unwrap_or("3000000");
    let nb_of_log_files: &str = matches.value_of("nb_of_log_files").unwrap_or("3");
    let maxfilesize: &str = matches.value_of("maxfilesize").unwrap_or("500000000");

    let rotate_over_size = rotate_over_size
        .parse::<u64>()
        .context("Cannot convert rotate_over_size value string into u64 integer !")?;
    let nb_of_log_files = nb_of_log_files
        .parse::<usize>()
        .context("Cannot convert nb_of_log_files value string into usize integer !")?;
    let maxfilesize = maxfilesize
        .parse::<u64>()
        .context("Cannot convert maxfilesize value string into u64 integer !")?;

    // Check that diode in and guichetin exist before going futher.
    if !Path::new(diode).exists()
        || !Path::new(diode).is_dir()
        || Path::new(diode).metadata()?.permissions().readonly()
    {
        panic!(
            "Path to diode-in {} not valid ! This should normaly be created by systemd.",
            diode
        )
    }
    if !Path::new(guichetin).exists()
        || !Path::new(guichetin).is_dir()
        || Path::new(guichetin).metadata()?.permissions().readonly()
    {
        panic!("Path to guichetin {} is not valid ! Please check if the directory exists and check the permissions.", guichetin);
    }
    // Check that the Clamav IP is valid.
    match clamav_ip.parse::<IpAddr>() {
        Ok(_) => (),
        Err(e) => panic!("The IP provided for Clamav is not valid ! {:?}", e),
    }

    // Check that log_path is valid.
    if !Path::new(log_path).exists()
        || !Path::new(log_path).is_dir()
        || Path::new(log_path).metadata()?.permissions().readonly()
    {
        panic!("Log path {} is not valid ! Please check if the directory exists and check the permissions.", log_path)
    }

    // Logger init and parameters.
    Logger::try_with_env_or_str("warn")?
        .log_to_file(FileSpec::default().directory(log_path))
        .format(opt_format)
        .duplicate_to_stderr(Duplicate::Warn)
        .rotate(
            Criterion::Size(rotate_over_size),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(nb_of_log_files),
        )
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    // Logger is set, so I can use it.
    info!("{}", "LeGuichet-In is starting");
    info!("Incoming Guichet : {}", guichetin);
    info!("In-Out diode folder path : {}", diode);
    info!("Logfile : {}", log_path);
    info!("Clamav IP : {}", clamav_ip);
    info!("Clamav port: {}", clamav_port);
    info!("Rotate logs over size : {:?}", rotate_over_size);
    info!("Number of logs to keep : {:?}", nb_of_log_files);

    // Housekeeping while starting up: removing everything or don't start
    leguichet::clean_guichet_dir(guichetin).context("Error in path GUICHETIN: Directory is not empty and cannot be cleaned, please check permissions.")?;
    leguichet::clean_guichet_dir(diode).context("Error in path DIODE: Directory is not empty and cannot be cleaned, please check permissions.")?;

    // Check if clamd is responding
    let _clam_result = leguichet::clamd_version(clamav_ip, clamav_port_int);

    // First scan begins here
    leguichet::clamd_scan(guichetin, clamav_ip, clamav_port_int)?;

    println!("Watching {} directory for activity...", guichetin);
    info!("Watching {} directory for activity...", guichetin);

    // Starting the main loop.
    loop {
        // Sanitize all forbidden files like dirs or .{yara|sha256}
        leguichet::sanitize_all(guichetin)?;
        //Hash the content
        leguichet::hash_folder_content(guichetin)?;
        // AV scanning
        leguichet::clamd_scan(guichetin, clamav_ip, clamav_port_int)?;
        // Remove files larger than maxfilesize and unwanted
        // extensions.
        leguichet::remove_toobig(guichetin, maxfilesize)?;
        // Finally, send those files.
        leguichet::send_clean_files(guichetin, diode)?;
        main_thread::sleep(Duration::from_millis(500));
    }
}
