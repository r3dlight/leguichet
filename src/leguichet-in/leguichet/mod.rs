// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "guichet-in".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains various funtions
 * for building the guichet-in binary.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

use crate::errors::*;
use clam_client::client::ClamClient;
use clam_client::response::ClamScanResult;
use crossbeam_utils::thread;
use nix::sys::stat;
use nix::unistd;
use regex::Regex;
use sha2::digest::consts::{B0, B1};
use sha2::digest::generic_array::typenum::{UInt, UTerm};
use sha2::digest::generic_array::GenericArray;
use sha2::{Digest, Sha256};
use std::cmp::Ordering;
use std::fs;
use std::fs::File;
use std::fs::{metadata, OpenOptions};
use std::io::Write;
use std::os::linux::fs::MetadataExt;
use std::path::{Path, PathBuf};
use syscallz::{Context as cont, Syscall};
use time::OffsetDateTime;
use walkdir::{DirEntry, WalkDir};
mod test_mod;

/// Call sha256_digest and write a .sha256 file containing the digest of the original file
///
/// # Example
///
/// ```
/// use tempfile::tempdir;
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// File::create(&file).unwrap();
/// create_sha256(&file).unwrap();
/// let sha256 = dir.path().join("file.txt.sha256");
/// assert_eq!(true, Path::new(sha256.to_str().unwrap()).exists());
/// ```
pub fn create_sha256(file: &Path) -> Result<()> {
    if file.exists() {
        let f: std::fs::Metadata = metadata(file)?;
        if f.is_file() {
            let digest = sha256_digest(&file.to_string_lossy())?;
            let mut path = PathBuf::new();
            path.push(file);
            match path.to_str() {
                Some(p) => {
                    let path_to_sha = format!("{}{}", p, ".sha256");
                    let _output = File::create(&path_to_sha)?;
                    let file = OpenOptions::new().write(true).open(path_to_sha)?;
                    writeln!(&file, "{:x}", digest)?;
                }
                //Should not happen
                None => error!("Cannot convert path to str to create .sha256 file"),
            }
        }
    } else {
        error!("File {:?} not found.", file);
    }
    Ok(())
}

/// Creates a sha256 hash from a file
pub fn sha256_digest(
    file: &str,
) -> Result<GenericArray<u8, UInt<UInt<UInt<UInt<UInt<UInt<UTerm, B1>, B0>, B0>, B0>, B0>, B0>>> {
    let mut open_file = File::open(file)?;
    let mut sha256: Sha256 = Sha256::new();
    std::io::copy(&mut open_file, &mut sha256)?;
    let hash = sha256.finalize();
    info!("File {} sha256 hash is: {:x}", file, hash);
    Ok(hash)
}

/// Gets metadata from a file and log it into default logger
///
/// # Example
///
/// ```
/// use tempfile::tempdir;
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// assert_eq!(get_metadata(file.to_str().unwrap()).is_ok(), true);
/// ```
pub fn get_metadata(filename: &str) -> Result<()> {
    if let Ok(metadata) = metadata(filename) {
        info!(
            "File owner for {} is uid {} with gid: {}.",
            filename,
            metadata.st_uid(),
            metadata.st_gid()
        );
    }
    Ok(())
}

/// Check the file size and remove it if the size is above MAXFILESIZE
///
/// # Example
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// let result = check_size(file, 0);
/// assert_eq!(false, file.exists());
/// ```
fn check_size(file: &str, maxfilesize: u64) -> Result<()> {
    let file_size: u64 = metadata(file)?.len();
    match file_size.cmp(&maxfilesize) {
        Ordering::Less | Ordering::Equal => {}
        Ordering::Greater => {
            remove(file)?;
            warn!("File {} is larger than MAXFILESIZE, removing it.", file);
            let path_to_size = format!("{}{}", file, ".toobig");
            let _output = File::create(&path_to_size)?;
            let file = OpenOptions::new().write(true).open(path_to_size)?;
            writeln!(&file, "File is bigger than the maximum allowed by your administrator: {} bytes. Removing...", file_size)?;
        }
    }
    Ok(())
}

/// This function removes the file if its extension is .yara or .sha256.
///
/// # Example
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let hash = dir.path().join("file.sha256");
/// let _output = File::create(&file).unwrap();
/// let _output = File::create(&hash).unwrap();
/// let result = sanitize(file);
/// assert_eq!(true, file.exists());
/// let result = sanitize(hash);
/// assert_eq!(false, hash.exists());
/// ```
fn sanitize(file: &str, guichetin: &str) -> Result<()> {
    if let Ok(n) = metadata(&file) {
        if n.is_dir() {
            if Path::new(file) != Path::new(guichetin) {
                match remove_dir(file) {
                    Ok(()) => warn!("Directory {} removed when starting up.", file),
                    Err(e) => error!(
                        "Cannot remove folder in guichet in after starting : {:?}",
                        e
                    ),
                }
            }
        } else if n.is_file() {
            if let Some(ext) = get_extension_from_file(file) {
                match ext {
                    "sha256" => {
                        remove(file)?;
                        warn!("Removing unallowed file with .sha256 extention: {}.", file);
                    }
                    "yara" => {
                        remove(file)?;
                        warn!("Removing unallowed file with .yara extention: {}.", file);
                    }
                    "antivirus" => {
                        remove(file)?;
                        warn!(
                            "Removing unallowed file with .antivirus extention: {}.",
                            file
                        );
                    }
                    "toobig" => {
                        remove(file)?;
                        warn!("Removing unallowed file with .toobig extention: {}.", file);
                    }
                    _ => (),
                }
            }
        } else {
        }
    } else {
        warn!("Cannot get metadata for file: {}", file);
    }
    Ok(())
}

/// This function removes every files or directories left while
/// starting up.
/// This function is multithreaded.
/// The path must be a valid a writable directory (&str)
///
/// # Example
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let hash = dir.path().join("file.sha256");
/// let _output = File::create(&file).unwrap();
/// let _output = File::create(&hash).unwrap();
/// let result = clean_guichet_dir(dir.path().to_str().unwrap());
/// assert_eq!(false, file.exists());
/// assert_eq!(false, hash.exists());
/// ```
pub fn clean_guichet_dir(path: &str) -> Result<()> {
    println!("Entering {} to clean files", &path);
    let names = list_files(path)?;
    thread::scope(|s| {
        for name in names {
            s.spawn(move |_| {
                let remove_me = format!("{}{}", path, name);
                if let Ok(n) = metadata(&remove_me.as_str()) {
                    if n.is_dir() {
                        match remove_dir(&remove_me) {
                            Ok(()) => {
                                warn!("Directory {} removed when starting up.", remove_me)
                            }
                            Err(e) => error!(
                                "Cannot remove folder in guichet in after starting : {:?}",
                                e
                            ),
                        }
                    } else if n.is_file() {
                        match remove(&remove_me) {
                            Ok(()) => warn!("File {} removed when starting up.", remove_me),
                            Err(e) => {
                                error!("Cannot remove file in guichet in after starting : {:?}", e)
                            }
                        }
                        warn!("File removed while starting {}", &remove_me)
                    } else {
                        match remove(&remove_me) {
                            Ok(()) => warn!("Diode {} removed when starting up.", remove_me),
                            Err(e) => error!("Cannot remove named pipes after starting : {:?}", e),
                        }
                    }
                } else {
                    error!("Cannot get metadata for file : {}", remove_me);
                    match remove(&remove_me) {
                        Ok(()) => warn!("File {} removed when starting up.", remove_me),
                        Err(e) => {
                            error!("Cannot remove file {} after starting : {:?}", remove_me, e)
                        }
                    }
                }
            });
        }
    })
    .expect("Cannot scope threads !");
    Ok(())
}

/// This function prints clamd version or panic.
/// Clamav is mandatory to start the daemon.
///
/// Example:
///
/// ```
/// assert_eq!(true, clamd_version("127.0.0.1", 3310).is_ok());
/// ```
pub fn clamd_version(clamav_ip: &str, clamav_port_int: u16) -> Result<()> {
    if let Ok(client) = ClamClient::new_with_timeout(clamav_ip, clamav_port_int, 5) {
        println!(
            "{:?}",
            client.version().expect(
                "ClamAV server is not responding ! Please check if it's correctly running."
            )
        );
        info!(
            "{:?}",
            client.version().expect(
                "ClamAV server is not responding ! Please check if it's correctly running."
            )
        );
    }
    Ok(())
}

/// get_extension_from_file(filename: &str)
/// This function gets the (fake) extension of a file and returns
/// an optional &str.
///
/// Example:
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// let extension = get_extension_from_file(file.to_str().unwrap());
/// assert_eq("txt", extension);
/// ```
pub fn get_extension_from_file(filename: &str) -> Option<&str> {
    debug!("Getting extension for : {}", filename);
    Path::new(filename).extension().and_then(|s| s.to_str())
}

/// This function scans files in a directory using Clamd.
/// You might want to set a clamd IP & port according to your needs.
///
/// Example:
///
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// let _result = clamd_scan(dir.path().to_str().unwrap(),"127.0.0.1", 3310)
/// ```
pub fn clamd_scan(guichetin: &str, clamav_ip: &str, clamav_port_int: u16) -> Result<()> {
    let client = ClamClient::new(clamav_ip, clamav_port_int)
        .context("Cannot create a new instance of ClamClient.")?;
    if let Ok(results) = client.scan_path(guichetin, true) {
        for scan_result in results.iter() {
            if let ClamScanResult::Found(location, virus) = scan_result {
                remove(location)?;
                println!("Clamav: Found virus: '{}' in {}", virus, location);
                warn!("{} Found virus: '{}' in {}", "Clamav:", virus, location);
                let path_to_report = format!("{}{}", location, ".antivirus");
                let _output = File::create(&path_to_report)?;
                let file = OpenOptions::new().write(true).open(path_to_report)?;
                writeln!(&file, "Malicious file detected: {}", virus)?;
            }
        }
    }
    Ok(())
}

/// This function returns a bool weither
/// a file is hidden or not.
/// This is a filter on regular files ans not hidden ones.
/// We do not want to catch .bashrc like files.
///
/// Example:
/// ```
/// let walker = WalkDir::new("foo").into_iter();
/// for entry in walker.filter_entry(|e| !is_not_hidden(e)) {
///    let entry = entry.unwrap();
///    println!("{}", entry.path().display());
/// ```
fn is_not_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| entry.depth() == 0 || !s.starts_with('.'))
        .unwrap_or(false)
}

/// This function recursively parse all files in a folder
/// and creates sha256 digest for each file.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// File::create(file).unwrap();
/// let tmpdir = format!("{}{}", &dir.path().to_str().unwrap(), "/");
/// hash_folder_content(&tmpdir).unwrap();
/// let shafile = dir.path().join("file.txt.sha256");
/// assert_eq!(true, Path::new(&shafile).exists());
/// ```
pub fn hash_folder_content(guichetin: &str) -> Result<()> {
    WalkDir::new(&guichetin)
        .into_iter()
        .filter_entry(|e| is_not_hidden(e))
        .filter_map(|v| v.ok())
        .for_each(|x| create_sha256(x.path()).unwrap());
    Ok(())
}

/// This function ecursively parse all files in a folder
/// to remove files larger than MAXFILESIZE value.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file");
/// File::create(&file).unwrap();
/// remove_toobig(dir.path().to_str().unwrap(), 10000).unwrap();
/// assert_eq!(true, file.exists());
/// ```
pub fn remove_toobig(guichetin: &str, maxfilesize: u64) -> Result<()> {
    WalkDir::new(&guichetin)
        .into_iter()
        .filter_entry(|e| is_not_hidden(e))
        .filter_map(|v| v.ok())
        .for_each(|x| check_size(x.path().to_str().unwrap(), maxfilesize).unwrap());
    Ok(())
}

/// This function recursively parse all files in a folder
/// and call sanitize on each file to remove files with
/// extensions matching .sha256 or .yara.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.sha256");
/// File::create(&file).unwrap();
/// assert_eq!(true, file.exists());
/// let file2 = dir.path().join("file.yara");
/// File::create(&file2).unwrap();
/// assert_eq!(true, file2.exists());
/// sanitize_all(dir.path().to_str().unwrap()).unwrap();
/// assert_eq!(false, file.exists());
/// assert_eq!(false, file2.exists());
/// ```
pub fn sanitize_all(guichetin: &str) -> Result<()> {
    WalkDir::new(&guichetin)
        .into_iter()
        .filter_entry(|e| is_not_hidden(e))
        .filter_map(|v| v.ok())
        .for_each(|x| sanitize(x.path().to_str().unwrap(), guichetin).unwrap());
    Ok(())
}

/// List clean files, creates a pipe for each one and call write_fifo
#[cfg(not(tarpaulin_include))]
pub fn send_clean_files(guichetin: &str, diode: &str) -> Result<()> {
    let names = list_files(guichetin)?;
    thread::scope(|s| {
        for name in names {
            let path_to_file = format!("{}{}", guichetin, name.as_str());
            let timestamp = format!(
                "{}-{}-{}_{}-{}-{}-{}",
                OffsetDateTime::now_utc().day(),
                OffsetDateTime::now_utc().month(),
                OffsetDateTime::now_utc().year(),
                OffsetDateTime::now_utc().hour(),
                OffsetDateTime::now_utc().minute(),
                OffsetDateTime::now_utc().second(),
                OffsetDateTime::now_utc().nanosecond()
            );
            //let timestamp = str::replace(&chrono::offset::Utc::now().to_string(), ":", "-");
            let timestamp = str::replace(&timestamp, " ", "-");
            let path_to_fifo = format!("{}{}-{}", diode, timestamp, name.as_str());
            let n: std::fs::Metadata =
                metadata(&path_to_file.as_str()).expect("Cannot get metadata in guichetin");
            debug!("Got metadata for file: {:?}", &path_to_file);
            s.spawn(move |_| {
                #[cfg(target_os = "linux")]
                init_seccomp_thread()
                    .expect("Cannot apply seccomp mode 2 syscall filter into thread");

                if n.is_dir() {
                    match remove_dir(path_to_file.as_str()) {
                        Ok(()) => {
                            debug!("Successfully removed directory: {}.", path_to_file.as_str())
                        }
                        Err(e) => error!("Cannot remove directory: {:?}", e),
                    }
                } else {
                    match get_metadata(&path_to_file) {
                        Ok(()) => debug!("Got metadata for {}.", path_to_file.as_str()),
                        Err(e) => error!("Problem while getting metadata: {:?}", e),
                    }
                    info!("Sending file: {}", name);

                    match mkfifo(path_to_fifo.as_str()) {
                        Ok(()) => debug!("Diode {} successfully created.", path_to_fifo.as_str()),
                        Err(e) => error!("Cannot create diode: {:?}", e),
                    }
                    //#[cfg(target_os = "linux")]
                    //init_seccomp_thread_write()
                    //    .expect("Cannot apply seccomp mode 2 syscall filter into thread");

                    match write_fifo(path_to_fifo.as_str(), &name, guichetin) {
                        Ok(()) => {
                            debug!("Successfully written into diode {}.", path_to_fifo.as_str())
                        }
                        Err(e) => error!("Error while creating the diode: {:?}", e),
                    }
                    match remove(path_to_fifo.as_str()) {
                        Ok(()) => debug!("Diode {} successfully removed", path_to_fifo.as_str()),
                        Err(e) => error!("Error while removing diode: {:?}", e),
                    }
                    match remove(path_to_file.as_str()) {
                        Ok(()) => debug!("File {} successfully removed", path_to_file.as_str()),
                        Err(e) => error!("Cannot remove file: {:?}", e),
                    }
                }
            });
        }
    })
    .unwrap();
    Ok(())
}

/// This function creates a named pipe.
/// Each name pipe is created with the name of the incoming file.
/// So each file has his own named pipe.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let path = dir.path().join("test_fifo");
/// mkfifo(&path.to_str().unwrap()).unwrap();
/// assert_eq!(true, Path::new(&path).exists());
/// remove(&path.to_str().unwrap()).unwrap();
/// assert_ne!(true, Path::new(&path).exists());
/// drop(dir)
/// ```
fn mkfifo(diode: &str) -> Result<()> {
    let fifo_path = PathBuf::from(diode);
    if Path::new(diode).exists() {
        info!("Cleaning the previous diode !!");
        remove(diode)?;
    }

    // Create a named pipe acting like a diode
    match unistd::mkfifo(&fifo_path, stat::Mode::S_IRGRP | stat::Mode::S_IWUSR) {
        Ok(_) => debug!("Name pipe created {:?}", fifo_path),
        Err(err) => error!("Error creating fifo: {}", err),
    }
    Ok(())
}

/// This function writes files into a named pipe which is
/// a blocking operation until it is read by another
/// process for example.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let filein = dir.path().join("file.in");
/// let fileout = dir.path().join("file.out");
/// File::create(&filein).unwrap();
/// fs::write(&filein, "test").unwrap();
/// File::create(&fileout).unwrap();
/// assert_eq!(true, filein.exists());
/// assert_eq!(true, fileout.exists());
/// write_fifo(
///    fileout.to_str().unwrap(),
///    "/file.in",
///    dir.path().to_str().unwrap(),
///).unwrap();
/// let digest = sha256_digest(filein.to_str().unwrap()).unwrap();
/// let digest2 = sha256_digest(fileout.to_str().unwrap()).unwrap();
///assert_eq!(digest, digest2,);
/// ```
fn write_fifo(diode: &str, file_name: &str, guichetin: &str) -> Result<()> {
    debug!("Writing file {} into the diode !", file_name);
    let path_to_file = format!("{}{}", guichetin, file_name);
    let data = std::fs::read(&path_to_file)?;
    fs::write(diode, data)?;
    warn!(
        "File {} passed successfully through the diode !",
        path_to_file
    );
    Ok(())
}

/// This function remove a file or a named pipe
/// Do not crash the daemon if permissions are
/// wrong and cannot remove the file !
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let file = dir.path().join("file.txt");
/// let _output = File::create(&file).unwrap();
/// remove(file.to_str().unwrap()).unwrap();
/// assert_eq!(false, Path::new(&file).exists());
/// ```
pub fn remove(file: &str) -> Result<()> {
    if Path::new(file).exists() {
        if fs::remove_file(file).is_ok() {
            debug!("Removing file : {}", file);
        } else {
            warn!(
                "Cannot remove file: {} ! Please remove it manually if still present.",
                file
            );
        }
    }
    Ok(())
}

/// This function removes a directory.
/// We try to avoid crashing the daemon is permissions
/// are wrong and cannot remove the directory.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let path = dir.path().join("directory");
/// let _output = fs::create_dir_all(&path).unwrap();
/// assert_eq!(true, Path::new(&path).exists());
/// remove_dir(path.to_str().unwrap()).unwrap();
/// assert_eq!(false, Path::new(&path).exists());
/// ```
pub fn remove_dir(dir: &str) -> Result<()> {
    if Path::new(dir).is_dir() {
        if fs::remove_dir_all(dir).is_ok() {
            debug!("Removing directory : {}", dir);
        } else {
            warn!(
                "Cannot remove directory: {} ! Please remove it manually if still present.",
                dir
            );
        }
    }
    Ok(())
}

/// This function lists all files in a directory except hidden ones.
///
/// Example:
/// ```
/// let dir = tempdir().unwrap();
/// let path = dir.path().join("transit");
/// let _output = fs::create_dir_all(&path).unwrap();
/// assert_eq!(true, Path::new(&path).exists());
/// let file = path.join("file.txt");
/// let _output = File::create(&file).unwrap();
/// assert_eq!(true, Path::new(&file).exists());
/// let files = list_files(path.to_str().unwrap());
/// assert_eq!(files.unwrap(), ["file.txt"]);
/// ```
pub fn list_files(directory: &str) -> Result<Vec<String>> {
    let paths: std::fs::ReadDir = fs::read_dir(directory)?;
    let mut names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(String::from))
            })
        })
        .collect::<Vec<String>>();
    // Not sending any files starting with dot like .bashrc
    let re = Regex::new(r"^\.([a-z])*")?;
    names.retain(|x| !re.is_match(x));
    Ok(names)
}

/// init_seccomp_thread()
/// This function applies a whitelist of syscalls
/// while starting a generic thread.
#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
pub fn init_seccomp_thread() -> Result<()> {
    let mut ctx = cont::init()?;
    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::write)?;
    #[cfg(not(target_arch = "arm"))]
    ctx.allow_syscall(Syscall::mmap)?;
    #[cfg(not(any(target_arch = "x86_64", target_arch = "aarch64")))]
    ctx.allow_syscall(Syscall::mmap2)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::exit)?;
    ctx.allow_syscall(Syscall::openat)?;
    ctx.allow_syscall(Syscall::statx)?;
    ctx.allow_syscall(Syscall::fstat)?;
    ctx.allow_syscall(Syscall::futex)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::mknod)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::mknodat)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::unlink)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::unlinkat)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::munmap)?;
    ctx.allow_syscall(Syscall::madvise)?;
    ctx.allow_syscall(Syscall::mprotect)?;
    ctx.allow_syscall(Syscall::mremap)?;
    ctx.allow_syscall(Syscall::getdents64)?;
    ctx.allow_syscall(Syscall::brk)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::rmdir)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::rename)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::renameat)?;
    ctx.allow_syscall(Syscall::clock_gettime)?;
    ctx.allow_syscall(Syscall::prctl)?;
    ctx.allow_syscall(Syscall::seccomp)?;
    ctx.allow_syscall(Syscall::ioctl)?;
    ctx.allow_syscall(Syscall::lseek)?;
    ctx.allow_syscall(Syscall::clone)?;
    ctx.allow_syscall(Syscall::set_robust_list)?;
    ctx.allow_syscall(Syscall::sched_getaffinity)?;
    ctx.load()?;
    Ok(())
}

/// init_seccomp_thread_write()
/// This function applies a lowered whitelist of syscalls
/// while starting a "write" thread.
#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
pub fn init_seccomp_thread_write() -> Result<()> {
    let mut ctx = cont::init()?;
    ctx.allow_syscall(Syscall::write)?;
    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::exit)?;
    ctx.allow_syscall(Syscall::clock_gettime)?;
    ctx.allow_syscall(Syscall::openat)?;
    ctx.allow_syscall(Syscall::futex)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::unlink)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::unlinkat)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::munmap)?;
    ctx.allow_syscall(Syscall::madvise)?;
    ctx.allow_syscall(Syscall::statx)?;
    ctx.allow_syscall(Syscall::mmap)?;
    ctx.allow_syscall(Syscall::mprotect)?;
    ctx.allow_syscall(Syscall::ioctl)?;
    ctx.allow_syscall(Syscall::brk)?;
    ctx.allow_syscall(Syscall::lseek)?;
    ctx.allow_syscall(Syscall::mremap)?;
    ctx.allow_syscall(Syscall::fstat)?;
    ctx.allow_syscall(Syscall::getdents64)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::rename)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::renameat)?;
    ctx.allow_syscall(Syscall::clone)?;
    ctx.allow_syscall(Syscall::set_robust_list)?;
    ctx.allow_syscall(Syscall::prctl)?;
    ctx.load()?;
    Ok(())
}
