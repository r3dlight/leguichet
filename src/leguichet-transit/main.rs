// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "guichet-transit".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains the main function.
 */

#![forbid(unsafe_code, private_in_public)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
//#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

#[macro_use]
extern crate log;

use clap::{arg, crate_version, Command};
use flexi_logger::{opt_format, Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
use std::path::Path;
use std::thread as main_thread;
use std::time::Duration;

mod errors;
mod sandbox;
use crate::errors::*;
mod leguichet;

#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    // Setting up allowed syscalls.
    #[cfg(target_os = "linux")]
    sandbox::init().context("Cannot apply first seccomp mode 2 syscall filter")?;

    // Start clap CLI definition
    let matches = Command::new("LeGuichet-Transit")
        .version(crate_version!())
        .author("r3dlight")
        .about("LeGuichet-Transit, transit window.")
        .arg(arg!( -g --guichettransit <PATH> "Sets le-guichet's path for transiting files").default_value("/var/local/transit/")
        )
        .arg(arg!( -i --diodein <PATH> "Sets the diode-in path for transiting files").default_value("/run/diode-in/")
        )
        .arg(arg!( -o --diodeout <PATH> "Sets the diode-out path for transfering files").default_value("/run/diode-out/")
        )
        .arg(arg!( -l --log <PATH> "Sets the path to logs").default_value("/var/log/leguichet-transit/")
        )
        .arg(arg!( -r --rules_path <PATH> "Sets a custom path for Yara rules").default_value("/usr/share/leguichet/rules/index.yar")
        )
        .arg(arg!( -t --yara_timeout <SECONDS> "Sets a custom timeout for libyara scans").default_value("900").validator(|s| s.parse::<u16>())
        )
        .arg(arg!( -m --yara_maxfilesize <SIZE> "Max file size for libyara to scan").default_value("50000000").validator(|s| s.parse::<u64>())
        )
        .arg(arg!( -c --yara_clean <BOOL> "Remove the file if at least one rule matchs").default_value("true").validator(|s| s.parse::<bool>())
        )
        .arg(arg!( -s --rotate_over_size <SIZE> "Rotate logs over specified size (bytes)").default_value("3000000").validator(|s| s.parse::<u64>())
        )
        .arg(arg!( -n --nb_of_log_files <NUMBER> "Number of log files to keep").default_value("3").validator(|s| s.parse::<usize>())
        )
        .arg(arg!( -a --allowed_formats <LIST> "Whitelist (comma separated) of allowed file formats").default_value("jpg,png,gif,bmp,mp4,m4v,avi,wmv,mpg,flv,mp3,wav,ogg,epub,mobi,doc,docx,xls,xlsx,ppt,pptx")
        )
        .get_matches();

    // Get values or use default ones.
    let guichet_transit = matches
        .value_of("guichet-transit")
        .unwrap_or("/var/local/transit/");
    let diode_in: &str = matches.value_of("diode-in").unwrap_or("/run/diode-in/");
    let diode_out: &str = matches.value_of("diode-out").unwrap_or("/run/diode-out/");
    let log_path: &str = matches
        .value_of("log")
        .unwrap_or("/var/log/leguichet-transit/");
    let rules_path: &str = matches
        .value_of("rules")
        .unwrap_or("/usr/share/leguichet/rules/index.yar");

    let yara_timeout: &str = matches.value_of("yara_timeout").unwrap_or("900");

    let yara_timeout = yara_timeout
        .parse::<u16>()
        .context("Cannot convert Yara timeout value string into u16 integer !")?;

    let yara_maxfilesize: &str = matches.value_of("yara_maxfilesize").unwrap_or("50000000");
    let yara_maxfilesize = yara_maxfilesize
        .parse::<u64>()
        .context("Cannot convert Yara max file size value string into u64 integer !")?;
    let rotate_over_size: &str = matches.value_of("rotate_over_size").unwrap_or("3000000");
    let nb_of_log_files: &str = matches.value_of("nb_of_log_files").unwrap_or("3");
    let yara_clean: &str = matches.value_of("yara_clean").unwrap_or("true");
    let yara_clean = yara_clean
        .parse::<bool>()
        .context("Cannot convert Yara_clean value string into boolean !")?;

    let rotate_over_size = rotate_over_size
        .parse::<u64>()
        .context("Cannot convert rotate_over_size value string into u64 integer !")?;
    let nb_of_log_files = nb_of_log_files
        .parse::<usize>()
        .context("Cannot convert nb_of_log_files value string into usize integer !")?;
    let magic_list = matches.value_of("allowed_formats").unwrap_or(
        "jpg,png,gif,bmp,mp4,m4v,avi,wmv,mpg,flv,mp3,wav,ogg,epub,mobi,doc,docx,xls,xlsx,ppt,pptx",
    );
    let magic_list = magic_list.split(',');
    let magic_list: Vec<&str> = magic_list.collect();

    // Check that diode_in, diode_out and guichet_transit exist before going futher.
    if !Path::new(diode_out).exists()
        || !Path::new(diode_out).is_dir()
        || Path::new(diode_out).metadata()?.permissions().readonly()
    {
        panic!(
            "Path to diode-out {} not valid ! This should normaly be created by systemd.",
            diode_out
        )
    }

    if !Path::new(diode_in).exists()
        || !Path::new(diode_in).is_dir()
        || Path::new(diode_in).metadata()?.permissions().readonly()
    {
        panic!(
            "Path to diode-in {} not valid ! This should normaly be created by systemd.",
            diode_in
        )
    }

    if !Path::new(guichet_transit).exists()
        || !Path::new(guichet_transit).is_dir()
        || Path::new(guichet_transit)
            .metadata()?
            .permissions()
            .readonly()
    {
        panic!("Path to transit guichet {} is not valid !", guichet_transit);
    }

    // Check that path to yara rules is valid.
    if !Path::new(rules_path).exists()
        || !Path::new(rules_path).is_file()
        || Path::new(rules_path).metadata()?.permissions().readonly()
    {
        panic!("Path to Yara index file ({}) is not valid !", rules_path)
    }

    // Check that log_path is valid.
    if !Path::new(log_path).exists()
        || !Path::new(log_path).is_dir()
        || Path::new(log_path).metadata()?.permissions().readonly()
    {
        panic!("Log path {} is not valid ! Please check if the directory exists and check the permissions.", log_path)
    }

    let allowed_magics: Vec<&str> = vec![
        "jpg", "png", "gif", "webp", "cr2", "tif", "bmp", "heif", "avif", "jxr", "psd", "ico",
        "mp4", "m4v", "mkv", "webm", "mov", "avi", "wmv", "mpg", "flv", "mid", "mp3", "m4a", "ogg",
        "flac", "wav", "amr", "aac", "epub", "zip", "tar", "rar", "gz", "bz2", "7z", "xz", "pdf",
        "swf", "rtf", "eot", "ps", "sqlite", "nes", "crx", "cab", "deb", "ar", "Z", "lz", "rpm",
        "dcm", "zst", "msi", "mobi", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "woff", "woff2",
        "ttf", "otf", "wasm", "exe", "dll", "elf", "bc", "mach", "class", "dex", "dey", "der",
        "obj", "sh", "xml", "html", "odt", "ods", "odp", "aiff", "dsf",
    ];
    for selected_type in magic_list.iter() {
        if !allowed_magics.contains(selected_type) {
            panic!("Unknown value in allowed_types: {}", selected_type);
        }
    }

    // Logger init and parameters.
    Logger::try_with_env_or_str("warn")?
        .log_to_file(FileSpec::default().directory(log_path))
        .format(opt_format)
        .duplicate_to_stderr(Duplicate::Warn)
        .rotate(
            Criterion::Size(rotate_over_size),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(nb_of_log_files),
        )
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    // Logger is set, so I can use it.
    info!("{}", "LeGuichet-Transit is starting");
    info!("Transit Guichet : {}", guichet_transit);
    info!("In diode path : {}", diode_in);
    info!("Out diode path : {}", diode_out);
    info!("Rotate logs over size : {:?}", rotate_over_size);
    info!("Number of logs to keep : {:?}", nb_of_log_files);
    info!("Yara max file size: {}", yara_maxfilesize);
    info!("Yara timeout: {}", yara_timeout);
    info!("Remove file if Yara matched: {}", yara_clean);
    info!("Allowed file formats: {:?}", magic_list);
    println!("Allowed file formats: {:?}", magic_list);

    // Housekeeping while starting up: removing everything
    leguichet::clean_guichet_dir(guichet_transit)
        .context("Cannot clean files in guichet transit path.")?;

    println!("Watching current directory for activity...");

    // Starting the main loop.
    loop {
        leguichet::read_files(guichet_transit, diode_in)?;
        main_thread::sleep(Duration::from_millis(120));
        leguichet::analyse_transit_files(
            guichet_transit,
            diode_out,
            rules_path,
            yara_timeout,
            yara_maxfilesize,
            yara_clean,
            magic_list.clone(),
        )?;
        leguichet::send_transit_files(guichet_transit, diode_out)?;
    }
}
