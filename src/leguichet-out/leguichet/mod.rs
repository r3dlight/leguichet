// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "guichet-out".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains various funtions
 * for building the guichet-in binary.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

use crate::errors::*;
use crossbeam_utils::thread;
use sha2::{Digest, Sha256};
use std::fs;
use std::fs::File;
use std::path::Path;
use syscallz::{Context as cont, Syscall};

mod test_mod;

/// Creates a sha256 hash a file
pub fn log_hash(file: &str) -> std::io::Result<()> {
    let mut open_file = File::open(file)?;
    let mut sha256: Sha256 = Sha256::new();
    std::io::copy(&mut open_file, &mut sha256)?;
    let hash = sha256.finalize();
    info!("File {} sha256 hash is: {:x}", file, hash);
    Ok(())
}

//Read from named pipe and write them to filesystem
pub fn read_files(guichetout: &str, diode: &str) -> Result<()> {
    let paths: std::fs::ReadDir = fs::read_dir(diode)?;
    let names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(String::from))
            })
        })
        .collect::<Vec<String>>();
    thread::scope(|s| {
        for name in names {
            s.spawn(move |_| {
                #[cfg(target_os = "linux")]
                init_seccomp_thread()
                    .expect("Cannot apply seccomp mode 2 syscall filter into thread");

                let path_to_file = format!("{}{}", diode, name.as_str());
                let path_to_file_written = format!("{}{}", guichetout, name.as_str());

                #[cfg(target_os = "linux")]
                init_seccomp_thread_write()
                    .expect("Cannot apply seccomp mode 2 syscall filter into thread");

                match write_file(path_to_file.as_str(), name.as_str(), guichetout) {
                    Ok(()) => debug!("write_file ok for file: {}.", path_to_file.as_str()),
                    Err(e) => error!(
                        "write_file: Error writing file {}: {:?}",
                        path_to_file.as_str(),
                        e
                    ),
                }
                match log_hash(&path_to_file_written) {
                    Ok(()) => debug!("log_hash ok for file {}", path_to_file_written),
                    Err(e) => error!("{:?}", e),
                }
            });
        }
    })
    .unwrap();
    Ok(())
}

fn write_file(path_to_file: &str, name: &str, guichetout: &str) -> Result<()> {
    let path_to_write = format!("{}{}", guichetout, name);
    if Path::new(path_to_file).exists() {
        let data = std::fs::read(&path_to_file)?;
        fs::write(&path_to_write, data)?;
        warn!(
            "File {} successfully written to outgoing guichet !",
            path_to_write
        );
    } else {
        warn!("[Error] write_file: path to diode doesn't exists anymore !");
    }
    Ok(())
}

#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
pub fn init_seccomp_thread() -> Result<()> {
    let mut ctx = cont::init()?;
    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::write)?;
    #[cfg(not(target_arch = "arm"))]
    ctx.allow_syscall(Syscall::mmap)?;
    #[cfg(not(any(target_arch = "x86_64", target_arch = "aarch64")))]
    ctx.allow_syscall(Syscall::mmap2)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::exit)?;
    ctx.allow_syscall(Syscall::openat)?;
    ctx.allow_syscall(Syscall::statx)?;
    ctx.allow_syscall(Syscall::futex)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::mknod)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::mknodat)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::unlink)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::unlinkat)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::munmap)?;
    ctx.allow_syscall(Syscall::madvise)?;
    ctx.allow_syscall(Syscall::mremap)?;
    ctx.allow_syscall(Syscall::brk)?;
    ctx.allow_syscall(Syscall::mprotect)?;
    ctx.allow_syscall(Syscall::clock_gettime)?;
    ctx.allow_syscall(Syscall::prctl)?;
    ctx.allow_syscall(Syscall::fcntl)?;
    ctx.allow_syscall(Syscall::fstat)?;
    ctx.allow_syscall(Syscall::getdents64)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::rename)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::renameat)?;
    ctx.allow_syscall(Syscall::clone)?;
    ctx.allow_syscall(Syscall::set_robust_list)?;
    ctx.allow_syscall(Syscall::sched_getaffinity)?;
    ctx.allow_syscall(Syscall::seccomp)?;
    ctx.allow_syscall(Syscall::lseek)?;
    ctx.load()?;
    Ok(())
}

#[cfg(not(tarpaulin_include))]
#[cfg(target_os = "linux")]
pub fn init_seccomp_thread_write() -> Result<()> {
    let mut ctx = cont::init()?;
    ctx.allow_syscall(Syscall::write)?;
    ctx.allow_syscall(Syscall::exit)?;
    ctx.allow_syscall(Syscall::clock_gettime)?;
    ctx.allow_syscall(Syscall::openat)?;
    ctx.allow_syscall(Syscall::futex)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::unlink)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::unlinkat)?;
    ctx.allow_syscall(Syscall::sigaltstack)?;
    ctx.allow_syscall(Syscall::munmap)?;
    ctx.allow_syscall(Syscall::madvise)?;
    ctx.allow_syscall(Syscall::statx)?;
    ctx.allow_syscall(Syscall::read)?;
    ctx.allow_syscall(Syscall::close)?;
    ctx.allow_syscall(Syscall::mremap)?;
    ctx.allow_syscall(Syscall::mmap)?;
    ctx.allow_syscall(Syscall::fcntl)?;
    ctx.allow_syscall(Syscall::fstat)?;
    ctx.allow_syscall(Syscall::getdents64)?;
    #[cfg(target_arch = "x86_64")]
    ctx.allow_syscall(Syscall::rename)?;
    #[cfg(target_arch = "aarch64")]
    ctx.allow_syscall(Syscall::renameat)?;
    ctx.allow_syscall(Syscall::clone)?;
    ctx.allow_syscall(Syscall::set_robust_list)?;
    ctx.allow_syscall(Syscall::prctl)?;
    ctx.allow_syscall(Syscall::sched_getaffinity)?;
    ctx.allow_syscall(Syscall::mprotect)?;
    ctx.allow_syscall(Syscall::lseek)?;
    ctx.load()?;
    Ok(())
}
