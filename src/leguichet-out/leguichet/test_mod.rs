// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "guichet-out".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains the tests.
 */

#[cfg(test)]
use super::*;

#[test]
fn test_log_hash() {
    use sha2::{Digest, Sha256};
    use std::fs::File;
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let file_path = dir.path().join("file_out_hash.txt");
    let _file: File = File::create(&file_path).unwrap();
    assert_eq!(true, Path::new(&file_path).exists());
    let mut open_file = File::open(&file_path).unwrap();
    let mut sha256 = Sha256::new();
    std::io::copy(&mut open_file, &mut sha256).unwrap();
    let hash = sha256.finalize();
    assert_eq!(hash[..], hash[0..32]);
    drop(&file_path);
    dir.close().unwrap();
}
#[test]
fn test_read_files() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path = dir.path().join("diodeout");
    let _output = fs::create_dir_all(&path).unwrap();
    let file = path.join("file.txt");
    let file2 = path.join("file2.txt");
    let file3 = path.join("file3.txt");
    let _output = File::create(&file).unwrap();
    let _output = File::create(&file2).unwrap();
    let _output = File::create(&file3).unwrap();
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&file2).exists());

    let path_guichetout = dir.path().join("guichetout");
    let _output = fs::create_dir_all(&path_guichetout).unwrap();

    let diode = format!("{}{}", &path.to_str().unwrap(), "/");
    let path_guichetout_string = format!("{}{}", &path_guichetout.to_str().unwrap(), "/");

    read_files(&path_guichetout_string, &diode).unwrap();
    let file = path_guichetout.join("file.txt");
    let file2 = path_guichetout.join("file2.txt");
    let file3 = path_guichetout.join("file3.txt");
    assert_eq!(true, Path::new(&file).exists());
    assert_eq!(true, Path::new(&file2).exists());
    assert_eq!(true, Path::new(&file3).exists());
    drop(dir);
}
#[test]
fn test_write_file() {
    use tempfile::tempdir;

    let dir = tempdir().unwrap();
    let path_diodeout = dir.path().join("diodeout");
    let path_guichetout = dir.path().join("guichetout");

    fs::create_dir_all(&path_diodeout).unwrap();
    assert_eq!(true, Path::new(&path_diodeout).exists());
    fs::create_dir_all(&path_guichetout).unwrap();
    assert_eq!(true, Path::new(&path_guichetout).exists());

    let file = &path_diodeout.join("file.txt");
    let _output = File::create(file).unwrap();
    assert_eq!(true, Path::new(file).exists());
    let guichetout = format!("{}{}", &path_guichetout.to_str().unwrap(), "/");
    write_file(file.to_str().unwrap(), "file.txt", &guichetout).unwrap();
    let newfile = &path_guichetout.join("file.txt");
    assert_eq!(true, Path::new(newfile).exists());
    drop(dir);
}
