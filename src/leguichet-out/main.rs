// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "guichet-out".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file contains the main function.
 */

#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

#[macro_use]
extern crate log;

use clap::{arg, crate_version, Command};
use flexi_logger::{opt_format, Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
use std::path::Path;
use std::thread as main_thread;
use std::time::Duration;

mod errors;
mod sandbox;
use crate::errors::*;
mod leguichet;

#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    // Setting up allowed syscalls.
    #[cfg(target_os = "linux")]
    sandbox::init()
        //.context("Failed to init sandbox")
        .context("Cannot apply first seccomp mode 2 syscall filter")?;

    // Start clap CLI definition
    let matches = Command::new("LeGuichet-Out")
        .version(crate_version!())
        .author("r3dlight")
        .about("LeGuichet-Out, output window.")
        .arg(
            arg!( -g --guichetout <PATH> "Sets the guichetout path for transfering files")
                .default_value("/var/local/out/"),
        )
        .arg(
            arg!( -d --diodeout <PATH> "Sets the diode-out path for transfering files")
                .default_value("/run/diode-out/"),
        )
        .arg(
            arg!( -l --log <PATH> "Sets a custom log path")
                .default_value("/var/log/leguichet-out/"),
        )
        .arg(
            arg!( -s --rotate_over_size <SIZE> "Rotate logs over specified size (bytes)")
                .default_value("3000000")
                .validator(|s| s.parse::<u64>()),
        )
        .arg(
            arg!( -n --nb_of_log_files <NUMBER> "Number of log files to keep")
                .default_value("3")
                .validator(|s| s.parse::<usize>()),
        )
        .get_matches();

    // Get values or use default ones.
    let guichetout: &str = matches.value_of("guichetout").unwrap_or("/var/local/out/");
    let diode: &str = matches.value_of("diode-out").unwrap_or("/run/diode-out/");
    let log_path: &str = matches.value_of("log").unwrap_or("/var/log/leguichet-out/");
    let rotate_over_size: &str = matches.value_of("rotate_over_size").unwrap_or("3000000");
    let nb_of_log_files: &str = matches.value_of("nb_of_log_files").unwrap_or("3");

    let rotate_over_size = rotate_over_size
        .parse::<u64>()
        .context("Cannot convert rotate_over_size value string into u64 integer !")?;
    let nb_of_log_files = nb_of_log_files
        .parse::<usize>()
        .context("Cannot convert nb_of_log_files value string into usize integer !")?;

    // I should parse each entry now.

    // Check that diode out and guichetout exist before going futher.
    if !Path::new(diode).exists()
        || !Path::new(diode).is_dir()
        || Path::new(diode).metadata()?.permissions().readonly()
    {
        panic!(
            "Path to diode-out {} not valid ! This should normaly be created by systemd.",
            diode
        )
    }

    if !Path::new(guichetout).exists()
        || !Path::new(guichetout).is_dir()
        || Path::new(guichetout).metadata()?.permissions().readonly()
    {
        panic!(
            "Path to guichetout {} is not valid ! Please check if the directory exists and check the permissions.",
            guichetout
        );
    }

    // Check that log_path is valid.
    if !Path::new(log_path).exists()
        || !Path::new(log_path).is_dir()
        || Path::new(log_path).metadata()?.permissions().readonly()
    {
        panic!("Log path {} is not valid ! Please check if the directory exists and check the permissions.", log_path)
    }

    // Logger init and parameters.
    Logger::try_with_env_or_str("warn")?
        .log_to_file(FileSpec::default().directory(log_path))
        .format(opt_format)
        .duplicate_to_stderr(Duplicate::Warn)
        .rotate(
            Criterion::Size(rotate_over_size),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(nb_of_log_files),
        )
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    // Logger is set, so I can use it.
    info!("{}", "LeGuichet-Out is starting");
    info!("Watching directory for files...");
    info!("Outgoing Guichet : {}", guichetout);
    info!("Out diode path : {}", diode);
    info!("Log Path : {}", log_path);
    info!("Rotate logs over size : {:?}", rotate_over_size);
    info!("Number of logs to keep : {:?}", nb_of_log_files);

    // Starting the main loop.
    loop {
        //Sleep a bit to save CPU
        main_thread::sleep(Duration::from_millis(120));
        //Read data from every mkfifo
        leguichet::read_files(guichetout, diode)?;
    }
}
