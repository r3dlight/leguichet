\selectlanguage{english} % valeurs possibles : french, english

\settitle[Le Guichet]{Le-Guichet:\\ A hardened decontamination gateway}

\setauthor[Stéphane Neveu]{Stéphane Neveu\\
  \email{stephane.neveu@ssi.gouv.fr}}

\institute{ANSSI}

\def\comRBE#1{\textcolor{blue}{{\bf [RBE]: #1}}}

\maketitle
\index{Stéphane Neveu}

\begin{abstract}
It is a complex task to choose and deploy a decontamination
solution that is sufficiently secured to transfer various files between two
different networks. This issue is even harder to solve when the solution has to
operate on virtual machines at a reasonable cost (close to zero).
The purpose of the current article is to present a tool called \emph{Le-Guichet}
which tries to fill this gap, offering an open source solution for a best effort
hardening using only software.         
\end{abstract}


\section{Introduction}

With the increasing complexity of information systems and network partitioning (for good security reasons),
users and administrators may need to exchange files between two or more networks. Exchanging files is not a trivial operation:
they could be vectors of contamination and spread to another user computer. Moreover, exchanging files 
between two distinct networks can potentially spread a threat to a network that was not previously compromised.

In Security Operation Centers, customers are usually required to provide documents or even log files to be analyzed by the security teams.
Thus, the exchange gateway must be as hardened as possible, it must guarantee one-way traffic and provide a solution to sanitize each transferred file.

Decontamination stations or antivirus terminals often require physical USB devices, which can be difficult to control and sanitize. Attacks like 
\emph{BadUSB}~\cite{Le-Guichet:badusb} are easy to mount and can compromise a lot of terminals without huge efforts. The antiviral scan often requires an internet access to communicate
with cloud hosted platforms, which is not always possible or even safe.
The use of USB devices is also time consuming and not really convenient: depending on the data, it sometimes could be difficult to perform transfers on an USB stick for an
antiviral analysis, and then copy it to the final target without any data alteration.


\section{Le-Guichet}

\subsection{Design}

\emph{Le-Guichet}~\cite{Le-Guichet:leguichet} is a modern decontamination proof of concept written in Rust~\cite{Le-Guichet:rust}, designed to be fast, multithreaded, with
security hardening in mind. It is composed of three different daemons performing their own tasks.
\newline
\textbf{Leguichet-in: }Untrusted files are dropped (via \texttt{rsync} over \texttt{ssh}) in the entry window (IN). The daemon asks an antivirus server (clamav for this demonstration) to automatically 
scan each dropped file. If a file is considered unhealthy, it is logged, hashed and immediately deleted. 
Files considered as healthy are also logged and hashed but sent (with associated SHA-256 digests) to the transit window (TRANSIT) through unidirectional 
software data diodes~\cite{Le-Guichet:diodes}. \emph{Le-Guichet} uses time-stamped \emph{named pipes}~\cite{Le-guichet:npipes} with strict Unix rights to reproduce the behavior of a 
hardware data diode with more flexibility. 
\newline
\textbf{Leguichet-transit: } This daemon reads the data diodes and write the files to the disks where they are logged and hashed again. 
A static analysis is performed on files using libyara~\cite{Le-Guichet:yara} and a report is generated if one rule or more is matched. 
Finally, files in transit are transferred to the output window (OUT) through another set of software diodes.
\newline
\textbf{Leguichet-out: } The last daemon is responsible for reading the available diodes and to write files, reports and digests in the output window.
\newline
Figure~\ref{fig:Le-Guichet:workflow} provides an overview of the workflow sending a file using \emph{le-Guichet}.

\begin{figure}[ht]
  \centering
  \ifssticbw
    \includegraphics[width=1.0\textwidth]{Le-Guichet/img/flowchart.png}
  \else
    \includegraphics[width=1.0\textwidth]{Le-Guichet/img/flowchart.png}
  \fi
  \caption{File transfer workflow}
  \label{fig:Le-Guichet:workflow}
\end{figure}
\subsection{Hardening} 
The hardening strategy is based on multiple foundations such as the chosen programming language, \emph{secure computing}~\cite{Le-Guichet:seccomp}, Systemd sandboxing and 
\emph{Mandatory Access Control}~\cite{Le-Guichet:mac}.
\newline
\newline
\textbf{The programming language:}
The choice of a programming language is critical regarding the overall security level. Languages such as C or C++
suffer from inherent security flaws.
We have decided to write \emph{Le-Guichet} using the Rust programming language.
Rust is a multi-paradigm programming language designed for performance and safety, especially safe
concurrency. It guarantees memory-safety by using a borrow check~\cite{Le-Guichet:borrow} to validate references without garbage collection.
\emph{Le-Guichet} is written without any use of \emph{Unsafe}~\cite{Le-guichet:unsafe} code blocks.
The \emph{Unsafe} keyword allows the use of five features which lower the security checks made by the compiler: 
\begin{itemize}
  \item Dereference a raw pointer;
  \item Call an unsafe function or method;
  \item Access or modify a mutable static variable;
  \item Implement an unsafe trait;
  \item Access fields of unions.
\end{itemize}

If the developer is not careful enough accessing the memory in a valid way, this can lead to security flaws.
As a preventive measure and because we do not need it, this feature is simply forbidden by design in the source code.
%\subsubsection{Secure computing:}
\newline
\textbf{Secure computing: }
Considering Rust's thread protection mechanisms are efficient, the code is also enforced using \emph{secure computing}~\cite{Le-Guichet:seccomp} mode 2 to limit the number of syscalls at runtime.
Each allowed syscall is whitelisted in the source code. 
\emph{Le-Guichet} supports both \texttt{x86\_64} and \texttt{aarch64} architectures. The initial syscall whitelist (at main thread startup) is later reduced for each thread spawn. 
This means that each thread has less allowed syscalls than the main process, ensuring a privilege drop.
\newline
\textbf{Systemd sandboxing: }
Systemd is a well known system and service manager, which is now used in almost every major GNU/Linux distribution.
\emph{Le-Guichet} takes advantage of all the protections offered by \emph{systemd.resource-control}~\cite{Le-Guichet:systemd.resource-control}. 
Here is an overall rating provided by the \emph{systemd-analyse security}~\cite{Le-Guichet:analyze} command which considers it \emph{safe}:
\begin{figure}[H]
  \centering
  \ifssticbw
    \includegraphics[width=0.74\textwidth]{Le-Guichet/img/systemd-security.png}
  \else
    \includegraphics[width=0.74\textwidth]{Le-Guichet/img/systemd-security.png}
  \fi
  \caption{systemd-analyse security}
  \label{fig:Le-Guichet:systemd}
\end{figure}
The command checks for various security-related service settings, assigning each a numeric "exposure level" value, depending on how important a setting is. 
It then calculates an overall exposure level for the whole unit, which is an estimation in the range 0.0...10.0 indicating how exposed a service is security-wise. 
High exposure levels indicate very little applied sandboxing. Low exposure levels indicate tight sandboxing and strongest security restrictions. 
Note that this only analyzes the per-service security features systemd itself implements. This means that any additional security mechanisms applied 
by the service code itself are not accounted for. The exposure level determined this way should not be misunderstood: 
a high exposure level neither means that there is no effective sandboxing applied by the service code itself, nor that the service is actually vulnerable 
to remote or local attacks. High exposure levels do indicate however that most likely the service might benefit from additional settings applied to them.
\newline
\textbf{Apparmor: }
Apparmor~\cite{Le-Guichet:apparmor} is a Mandatory Access Control~\cite{Le-Guichet:mac} system implemented upon the Linux Security Modules.
It proactively protects the Operating System and applications from 
external or internal threats, even zero-day attacks, by enforcing good behavior and preventing both known and unknown application flaws from being exploited.
Each daemon provided by \emph{Le-Guichet} is enforced with an \emph{Apparmor} protection profile. SELinux~\cite{Le-Guichet:selinux} support is in progress, but for now, only Apparmor is 
fully supported.
\\As an example, here is the Apparmor profile used for the binary named \emph{leguichet-out}:
\begin{lstlisting}[language={},caption={Leguichet-out's Apparmor profile},label={lst:Le-Guichet:appprofile}]
/usr/bin/leguichet-out {
  #include <abstractions/base>
  /run/diode-out/ r,
  /run/diode-out/** r,
  owner /var/local/out/ r,
  owner /var/local/out/** rw,
  owner /var/log/leguichet-out/ r,
  owner /var/log/leguichet-out/** rw,
}
\end{lstlisting}
The above profile shows that the corresponding daemon is owner of the paths \texttt{/var/local/out/} and \texttt{/var/log/leguichet-out/} and is allowed to read and write these paths. On the other hand,
leguichet-out is not owner of \texttt{/run/diode-out/} however this path can be read only by the daemon.

\section{Conclusion}

\emph{Le-Guichet} provides an alternative and open source solution allowing small and medium businesses to safely transfer various file types between different networks. 
The solution does not fit for classified networks due to the data diodes that are not separated with dedicated hardware. Nonetheless, named pipes implementation is quite 
easy to maintain and an effort is made to keep it as safe as possible using strict Unix rights. User documentation provides advice on hardening the Operating System 
hosting \emph{Le-Guichet} and citing various recommendations from \emph{ANSSI}~\cite{Le-Guichet:linux} \cite{Le-Guichet:ssh}.
In order to meet higher security requirements, it is likely that this work will lead to study the replacement of the named pipes by a custom hardware based on FPGAs.
This will lead to two different versions of \emph{Le-Guichet}, one purely software and one with dedicated hardware separation so that a compromise of the server 
hosting \emph{Le-Guichet} cannot result in a complete compromise of the whole gateway.
\newpage
\bibliography{Le-Guichet/biblio}

