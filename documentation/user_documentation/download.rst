********
Download
********

Lastest release
==================

Le-Guichet v0.5.0
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `leguichet-v0.5.0.zip <https://le-guichet.com/download/leguichet-v0.5.0.zip>`_
 * `leguichet-v0.5.0.zip.sig <https://le-guichet.com/download/leguichet-v0.5.0.zip.sig>`_
 * `leguichet-v0.5.0.zip.sha256 <https://le-guichet.com/download/leguichet-v0.5.0.zip.sha256>`_
 * `leguichet-v0.5.0-testing.zip <https://le-guichet.com/download/leguichet-v0.5.0-testing.zip>`_
 * `leguichet-v0.5.0-testing.zip.sig <https://le-guichet.com/download/leguichet-v0.5.0-testing.zip.sig>`_
 * `leguichet-v0.5.0-testing.zip.sha256 <https://le-guichet.com/download/leguichet-v0.5.0-testing.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Shell scripts overhaul;
 * Update syscall whitelist for Aarch64;
 * Ring crate removed (only sha2 is used now);
 * Error context improvement using anyhow.

Previous releases
==================

Le-Guichet v0.4.9
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `leguichet-v0.4.9.zip <https://le-guichet.com/download/leguichet-v0.4.9.zip>`_
 * `leguichet-v0.4.9.zip.sig <https://le-guichet.com/download/leguichet-v0.4.9.zip.sig>`_
 * `leguichet-v0.4.9.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.9.zip.sha256>`_
 * `leguichet-v0.4.9-testing.zip <https://le-guichet.com/download/leguichet-v0.4.9-testing.zip>`_
 * `leguichet-v0.4.9-testing.zip.sig <https://le-guichet.com/download/leguichet-v0.4.9-testing.zip.sig>`_
 * `leguichet-v0.4.9-testing.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.9-testing.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Update USB mode scripts and documentation;
 * Update dependencies.


Le-Guichet v0.4.8
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `leguichet-v0.4.8.zip <https://le-guichet.com/download/leguichet-v0.4.8.zip>`_
 * `leguichet-v0.4.8.zip.sig <https://le-guichet.com/download/leguichet-v0.4.8.zip.sig>`_
 * `leguichet-v0.4.8.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.8.zip.sha256>`_
 * `leguichet-v0.4.8-testing.zip <https://le-guichet.com/download/leguichet-v0.4.8-testing.zip>`_
 * `leguichet-v0.4.8-testing.zip.sig <https://le-guichet.com/download/leguichet-v0.4.8-testing.zip.sig>`_
 * `leguichet-v0.4.8-testing.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.8-testing.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Add more controls on parsing the magic number whitelist;
 * Update function documentation;
 * Update dependencies.

Le-Guichet v0.4.7
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `leguichet-v0.4.7.zip <https://le-guichet.com/download/leguichet-v0.4.7.zip>`_
 * `leguichet-v0.4.7.zip.sig <https://le-guichet.com/download/leguichet-v0.4.7.zip.sig>`_
 * `leguichet-v0.4.7.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.7.zip.sha256>`_
 * `leguichet-v0.4.7-testing.zip <https://le-guichet.com/download/leguichet-v0.4.7-testing.zip>`_
 * `leguichet-v0.4.7-testing.zip.sig <https://le-guichet.com/download/leguichet-v0.4.7-testing.zip.sig>`_
 * `leguichet-v0.4.7-testing.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.7-testing.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Update crates;
 * Tests and release for Debian 11 (Bullseye/testing);
 * Minor bug fix: Handle unsupported file types.

Le-Guichet v0.4.6
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `leguichet-v0.4.6.zip <https://le-guichet.com/download/leguichet-v0.4.6.zip>`_
 * `leguichet-v0.4.6.zip.sig <https://le-guichet.com/download/leguichet-v0.4.6.zip.sig>`_
 * `leguichet-v0.4.6.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.6.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Users can now create a white list of file types allowed to be transfered (docx, jpg, rpm, deb, etc...);
 * Update dependencies.

Le-Guichet v0.4.5
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `leguichet-v0.4.5.zip <https://le-guichet.com/download/leguichet-v0.4.5.zip>`_
 * `leguichet-v0.4.5.zip.sig <https://le-guichet.com/download/leguichet-v0.4.5.zip.sig>`_
 * `leguichet-v0.4.5.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.5.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Switch to clap-v3;
 * Remove colored crate for a better code coverage.


Le-Guichet v0.4.4
~~~~~~~~~~~~~~~~~

.. admonition:: Files
 :class: tip

 * `leguichet-v0.4.4.zip <https://le-guichet.com/download/leguichet-v0.4.4.zip>`_
 * `leguichet-v0.4.4.zip.sig <https://le-guichet.com/download/leguichet-v0.4.4.zip.sig>`_
 * `leguichet-v0.4.4.zip.sha256 <https://le-guichet.com/download/leguichet-v0.4.4.zip.sha256>`_

.. admonition:: Release note
 :class: note

 * Remove Inotify as it wasn't well maintained;
 * Bump to infer 0.4.0;
 * Remove notify related syscalls.

