**************
Administration
**************

Design
======

**Le-Guichet** provides three different daemons running at the same time and three different configuration files to be able to configure each daemon.
The following schema explains the logic behind **Le-guichet**.

.. image:: img/flowchart.png

Untrusted files are deposited (via rsync over ssh) in the entry window (in) and automatically scanned by an antivirus server.
If a file is considered unhealthy, it is logged/hashed and immediately deleted. 

Files considered as healthy are also logged and hashed (sha256) but sent (with associated hashes) to the transit window 
through several (one per file) unidirectional software diode (named pipe).
A static analysis is performed on each file using libyara and a report is generated if one or more rules matched. 

Finally, files in transit and any reports generated are transfered to the output window through another software diode subsystem.

System configuration
====================

Recommendations
---------------

Here are some recommendations and best practices to use **Le-Guichet** in good conditions.

.. admonition:: System hardening recommendations.

 To be safe enough, the GNU/Linux system used should be hardened according to the best practices such as *ANSSI*, *CIS*, *STIG* etc.

 You can take a look at the following: https://www.ssi.gouv.fr/en/guide/configuration-recommendations-of-a-gnulinux-system/ 

.. admonition:: Network Cards
 :class: tip

 We recommend to set up at least four network cards:
  * One dedicated card and IP address for the untrusted network;
  * One dedicated card and IP address for the trusted network;
  * One dedicated card and IP address for the administration tasks;
  * One dedicated card and IP address for Syslog.

It is also highly recommended to preserve an SSH access using a dedicated daemon and a dedicated network.

.. admonition:: OpenSSH configuration
 :class: tip

 You can set up three separate OpenSSH daemons:
  * One for the *administration* network;
  * One for the *untrusted* network;
  * One for the *trusted* network.

 Alternatively, you can choose to set up only two separate OpenSSH daemons:
  * One for the *administration* network;
  * One for the *untrusted* network and the *trusted* network.

 **In any cases, you should configure allowed users and networks in your sshd configuration:**

 .. code-block:: bash

   # OpenSSH daemon listening on the *untrusted_network*:
   AllowUsers untrusted_user@untrusted_network untrusted_user2@untrusted_network

 .. code-block:: bash

   # OpenSSH daemon listening on the *trusted_network*:
   AllowUsers trusted_user@trusted_network trusted_user2@trusted_network

 .. code-block:: bash

   # OpenSSH daemon listening on the *administration_network*:
   AllowUsers admin_user@administration_network admin_user2@administration_network

 Explanations about creating *untrusted_users* and *trusted_users* can be found in the :ref:`Usage` section.

Leguichet-in
--------------

**Leguichet-in** daemon is responsible for scanning files in the entry point directory, using an antivirus signature database and producing a *sha256* digest for each file.
The ELF binary is installed under:

.. code-block:: shell-session

  /usr/bin/leguichet-in

The configuration file for this daemon is:

.. code-block:: shell-session

  /etc/leguichet/leguichet-in.conf

The corresponding log directory is:

.. code-block:: shell-session

  /var/log/leguichet-in/

Let's take a look at the configuration:

.. code-block:: shell-session

 $ view /etc/leguichet/leguichet-in.conf

It should look like this:

.. code-block:: bash

 # Leguichet-in configuration file
 # This file is part of Le-Guichet
 #
 # Clamd server IP 
 # Note that if you modify this address, you also
 # have to edit the following file 
 # /etc/systemd/system/leguichet-in.service.d/leguichet-in.conf
 # to allow sockets via systemd.
 # See https://le-guichet.com/configuration.html#systemd
 # for more information.
 CLAMAV_IP=127.0.0.1
 
 # Clamd server port
 CLAMAV_PORT=3310
 
 # Diode-in directory
 # You should not touch this parameter.
 DIODEIN=/run/diode-in/
 
 # Path where incoming files will be deposited
 # You should not touch this parameter.
 GUICHETIN=/var/local/in/
 
 # Path to log directory
 # You should not touch this parameter.
 LOG=/var/log/leguichet-in/
 
 # Maximum size in bytes before rotating current log
 ROTATE_OVER_SIZE=3000000
 
 # How many log file to keep under $LOG path
 NB_OF_LOG_FILES=3
 
 # Maximum file size to transfert
 # Default is 500Mo
 MAXFILESIZE=500000000

.. warning::
 Do not modify **DIODE-IN**, **GUICHETIN** and **LOG** parameters unless you really know what to do.

You might want to ajust **CLAMAV_IP**, **CLAMAV_PORT**, **ROTATE_OVER_SIZE**, **NB_OF_LOG_FILES** and **MAXFILESIZE** according to your needs.

CLAMAV_IP
~~~~~~~~~

As commented in the configuration file, this is the IP address where the *Clamav* daemon will be listening to. 
If *Clamav* is installed locally, you have no need to modify this parameter.
Port 3310 is the default port for both Clamav and **Le-guichet**.
If the Clamav daemon is running somewhere else, change the address here:

.. code-block:: shell-session

 CLAMAV_IP=192.168.1.43

.. note::
 In order, the make this change efficient, you also need to edit the corresponding *Systemd* unit to allow tcp sockets for the new address.
 See the :ref:`Systemd unit files` section for more details.


ROTATE_OVER_SIZE
~~~~~~~~~~~~~~~~

This parameter represents the maximum log size in bytes before automatically rotating the current log in path /var/log/leguichet-in/.

NB_OF_LOG_FILES
~~~~~~~~~~~~~~~

This parameter represents the number of log files to keep in path /var/log/leguichet-in/.

MAXFILESIZE
~~~~~~~~~~~

This parameter represents the maximum file size (in bytes) allowed to be transfered. If a file is bigger than **MAXFILESIZE**, it is deleted.

.. note::

 Files bigger than MAXFILESIZE will be automatically removed.


Leguichet-transit
------------------

**Leguichet-transit** is mainly responsible for removing forbidden file format based on their **magic numbers** and scanning
incoming files with *libyara*.


The ELF binary is installed under:

.. code-block:: shell-session

  /usr/bin/leguichet-transit

The configuration file for this daemon is:

.. code-block:: shell-session

  /etc/leguichet/leguichet-transit.conf

The corresponding log directory is:

.. code-block:: shell-session

  /var/log/leguichet-transit/

Let's now take a look at the configuration of the seconf daemon called *leguichet-transit*:

.. code-block:: shell-session

 $ view /etc/leguichet/leguichet-transit.conf

It should look like this:

.. code-block:: bash

 # Leguichet-transit configuration file
 # This file is part of Le-Guichet.
 #
 # Diode-in directory
 # You should not touch this parameter.
 DIODEIN=/run/diode-in/
 
 # Diode-out directory
 # You should not touch this parameter.
 DIODEOUT=/run/diode-out/
 
 # Path where for files in transit
 # You should not touch this parameter.
 TRANSIT=/var/local/transit/
 
 # Path to log directory
 # You should not touch this parameter.
 # Pay attention to add a slash at the end
 LOG=/var/log/leguichet-transit/
 
 # Path to Yara rules (don't forget to add index.yar)
 RULES=/usr/share/leguichet/rules/index.yar
 
 # Yara max file size to scan
 # The bigger it is, the longer it takes to scan a file !
 # Default is 50Mo (50000000 bytes)
 YARA_MAXFILESIZE=50000000
 
 # Yara timeout when scannning files
 YARA_TIMEOUT=1000
 
 # Tells if LeGuichet should remove the file if Yara matched at least one rule
 YARA_CLEAN=true
 
 # Maximum size in bytes before rotating current log
 ROTATE_OVER_SIZE=3000000
 
 # How many log file to keep under $LOG path
 NB_OF_LOG_FILES=3

 # Set here a whitelist (comma separated) of allowed file formats
 # For example:
 # ALLOWED_FORMATS="deb,rpm"
 # See https://le-guichet.com/administration.html#leguichet-transit for more information.
 ALLOWED_FORMATS="jpg,png,bmp,mp4,m4v,avi,wmv,mpg,flv,mp3,wav,ogg,epub,mobi,doc,docx,xls,xlsx,ppt,pptx"


.. warning::
 Do not modify **DIODEIN**, **DIODEOUT**, **TRANSIT** and **LOG** parameters unless you really know what to do.

You might want to ajust **YARA_MAXFILESIZE**, **YARA_TIMEOUT**, **YARA_CLEAN**, **ROTATE_OVER_SIZE** and **NB_OF_LOG_FILES** according to your needs.

YARA_MAXFILESIZE
~~~~~~~~~~~~~~~~

This parameter sets the maximum file size (in bytes) to be scanned. The bigger it is, the longer it takes to scan a file !
You should set this option to the same value as MAXFILESIZE (leguichet-in) to be consistant.
If a file is bigger than YARA_MAXFILESIZE, it is deleted.

YARA_TIMEOUT
~~~~~~~~~~~~

This parameter sets a timeout (in seconds) to scan a file. 
If a file scan takes too long because of a big file, you can adjust the timeout here.

YARA_CLEAN
~~~~~~~~~~

This parameter tells if **Le-Guichet** should remove the file if Yara matched at least one rule.

ROTATE_OVER_SIZE
~~~~~~~~~~~~~~~~

This parameter represents the the maximum log size in bytes before automatically rotating the current log in path /var/log/leguichet-in/.

NB_OF_LOG_FILES
~~~~~~~~~~~~~~~

This parameter represents the number of log files to keep in path /var/log/leguichet-in/.

ALLOWED_TYPES
~~~~~~~~~~~~~~~

This parameter creates a whitelist of allowed file types. Types not explicitly listed here simply won't by transfered. For now the following types are supported:

.. admonition:: Supported types
 :class: tip

 **Image**

 - **jpg** - `image/jpeg`
 - **png** - `image/png`
 - **gif** - `image/gif`
 - **webp** - `image/webp`
 - **cr2** - `image/x-canon-cr2`
 - **tif** - `image/tiff`
 - **bmp** - `image/bmp`
 - **heif** - `image/heif`
 - **avif** - `image/avif`
 - **jxr** - `image/vnd.ms-photo`
 - **psd** - `image/vnd.adobe.photoshop`
 - **ico** - `image/vnd.microsoft.icon`


 **Video**

 - **mp4** - `video/mp4`
 - **m4v** - `video/x-m4v`
 - **mkv** - `video/x-matroska`
 - **webm** - `video/webm`
 - **mov** - `video/quicktime`
 - **avi** - `video/x-msvideo`
 - **wmv** - `video/x-ms-wmv`
 - **mpg** - `video/mpeg`
 - **flv** - `video/x-flv`


 **Audio**

 - **mid** - `audio/midi`
 - **mp3** - `audio/mpeg`
 - **dsf** - `audio/x-dsf`
 - **m4a** - `audio/m4a`
 - **ogg** - `audio/ogg`
 - **flac** - `audio/x-flac`
 - **wav** - `audio/x-wav`
 - **amr** - `audio/amr`
 - **aac** - `audio/aac`
 - **aiff**- `audio/aiff`


 **Archive**

 - **epub** - `application/epub+zip`
 - **zip** - `application/zip`
 - **tar** - `application/x-tar`
 - **rar** - `application/vnd.rar`
 - **gz** - `application/gzip`
 - **bz2** - `application/x-bzip2`
 - **7z** - `application/x-7z-compressed`
 - **xz** - `application/x-xz`
 - **pdf** - `application/pdf`
 - **swf** - `application/x-shockwave-flash`
 - **rtf** - `application/rtf`
 - **eot** - `application/octet-stream`
 - **ps** - `application/postscript`
 - **sqlite** - `application/vnd.sqlite3`
 - **nes** - `application/x-nintendo-nes-rom`
 - **crx** - `application/x-google-chrome-extension`
 - **cab** - `application/vnd.ms-cab-compressed`
 - **deb** - `application/vnd.debian.binary-package`
 - **ar** - `application/x-unix-archive`
 - **Z** - `application/x-compress`
 - **lz** - `application/x-lzip`
 - **rpm** - `application/x-rpm`
 - **dcm** - `application/dicom`
 - **zst** - `application/zstd`
 - **msi** - `application/x-ole-storage`


 **Book**

 - **epub** - `application/epub+zip`
 - **mobi** - `application/x-mobipocket-ebook`


 **Documents**

 - **doc** - `application/msword`
 - **docx** - `application/vnd.openxmlformats-officedocument.wordprocessingml.document`
 - **xls** - `application/vnd.ms-excel`
 - **xlsx** - `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`
 - **ppt** - `application/vnd.ms-powerpoint`
 - **pptx** - `application/vnd.openxmlformats-officedocument.presentationml.presentation`
 - **odt** - `application/vnd.oasis.opendocument.text`
 - **ods** - `application/vnd.oasis.opendocument.spreadsheet`
 - **odp** - `application/vnd.oasis.opendocument.presentation`


 **Font**

 - **woff** - `application/font-woff`
 - **woff2** - `application/font-woff`
 - **ttf** - `application/font-sfnt`
 - **otf** - `application/font-sfnt`


 **Application**

 - **wasm** - `application/wasm`
 - **exe** - `application/vnd.microsoft.portable-executable`
 - **dll** - `application/vnd.microsoft.portable-executable`
 - **elf** - `application/x-executable`
 - **bc** - `application/llvm`
 - **mach** - `application/x-mach-binary`
 - **class** - `application/java`
 - **dex** - `application/vnd.android.dex`
 - **dey** - `application/vnd.android.dey`
 - **der** - `application/x-x509-ca-cert`
 - **obj** - `application/x-executable`

 **Text**

 - **sh** - `text/x-shellscript`
 - **xml** - `text/xml`
 - **html** - `text/html`


 **Known Issues**

 - `exe` and `dll` have the same magic number so it's not possible to tell which one just based on the binary data. `exe` is returned for all.


Leguichet-out
--------------

The last daemon called **leguichet-out** is only responsible for retreiving incoming files from the software data diodes and writing them on the directory **GUICHETOUT**.

The ELF binary is installed under:

.. code-block:: shell-session

  /usr/bin/leguichet-out

The configuration file for this daemon is:

.. code-block:: shell-session

  /etc/leguichet/leguichet-out.conf

The corresponding log directory is:

.. code-block:: shell-session

  /var/log/leguichet-out/

Finally, here is the configuration of the last daemon called *leguichet-out*:

.. code-block:: shell-session

 /etc/leguichet/leguichet-out.conf

It should look like this:

.. code-block:: bash

 # Leguichet-out configuration file
 # This file is part of Le-Guichet
 #
 # Diode directory
 # You should not touch this parameter.
 # Pay attention to add a slash at the end
 DIODEOUT=/run/diode-out/
 
 # Path where incoming files will be deposited
 # You should not touch this parameter.
 # Pay attention to add a slash at the end
 GUICHETOUT=/var/local/out/
 
 # Path to log directory
 # You should not touch this parameter.
 # Pay attention to add a slash at the end
 LOG=/var/log/leguichet-out/
 
 # Maximum size in bytes before rotating current log
 ROTATE_OVER_SIZE=3000000
 
 # How many log file to keep under $LOG
 NB_OF_LOG_FILES=3

.. warning::
  You should not modify **DIODEOUT**, **GUICHETOUT** and **LOG** parameters. 

You can ajust **ROTATE_OVER_SIZE** and **NB_OF_LOG_FILES** like previously discussed.

ROTATE_OVER_SIZE
~~~~~~~~~~~~~~~~

This parameter represents the the maximum log size in bytes before automatically rotating the current log in path /var/log/leguichet-out/.

NB_OF_LOG_FILES
~~~~~~~~~~~~~~~

This parameter represents the number of log files to keep in path /var/log/leguichet-out/.

Systemd unit files
------------------

We won't discuss here how **Le-Guichet**'s systemd hardening is made, as it is not much interesting. We will simply explain how to reconfigure **leguichet-in**'s unit if you need to run the Clamav daemon on another server. 

*Systemd* units are splitted into two differrent files. In case of **leguichet-in**:

.. code-block:: shell-session

 /etc/systemd/system/leguichet-in.service

This fragment contains the basic configuration of the unit. You do not need to modify this one.

And :

.. code-block:: shell-session

 /etc/systemd/system/leguichet-in.service.d/security.conf

This is where comes the hardening part of the unit. The security.conf file is a drop-in systemd file. It is automatically concatenated with the fragment part of the unit.
You can see the entire resulting unit using the following command:

.. code-block:: shell-session

 $ systemctl cat leguichet-in

If you want to allow **leguichet-in** to communicate with a *Clamav* server listening on IP 192.168.1.43:

.. admonition:: Edit the Systemd unit
 :class: note
  
 .. code-block:: shell-session

   #/etc/systemd/system/leguichet-in.service.d/security.conf
   IPAddressAllow=127.0.0.1/8

 Change the above parameter with:

 .. code-block:: shell-session

   #/etc/systemd/system/leguichet-in.service.d/security.conf
   IPAddressAllow=192.168.1.43/32


.. warning::

 Do not forget to provide a netmask, Systemd requires it !


Then, reload the daemon:

.. code-block:: shell-session

 $ sudo systemctl daemon-reload

and restart **leguichet**:

.. code-block:: shell-session

 $ sudo systemctl restart leguichet

And that's it, you're all done !

Here is the security result achieved by default according to the **systemd analyse-security** command:

.. image:: /img/systemd-security.png
 
Apparmor
--------

From *Wikipedia* :

"AppArmor (Application Armor) is a Linux kernel security module that allows the system administrator to restrict 
programs capabilities with per-program profiles. Profiles can allow capabilities like network access, raw socket access, 
and the permission to read, write, or execute files on matching paths."

.. note::

 Although we are working on supporting SELinux, **Le-guichet** is only providing Apparmor profiles at this time.  

**Apparmor** profiles are located here:

 * /etc/apparmor.d/usr.bin.leguichet-in
 * /etc/apparmor.d/usr.bin.leguichet-transit
 * /etc/apparmor.d/usr.bin.leguichet-out

You will probably never have to modify them (it is not recommended anyway). Nevertheless, in case you need to update them, do not forget to reload the changed profile:

.. code-block:: shell-session

 $ sudo apparmor_parser -r /etc/apparmor.d/usr.bin.leguichet-in

Then, verify that the profile is still in **enforce mode**:

.. code-block:: shell-session

 $ sudo aa-status
 9 processes are in enforce mode.
   /usr/bin/freshclam (1580) 
   /usr/bin/leguichet-in (433022) 
   /usr/bin/leguichet-out (433027) 
   /usr/bin/leguichet-transit (433025) 
   /usr/sbin/clamd (966)
   ...

Static analysis
---------------

**Le-Guichet-transit** daemon is able to perform a *Yara* scan on transfered files according to the rules defined in path:

.. code-block:: shell-session

  /usr/share/leguichet/rules/index.yar

This file can be a simple rule like provided in the default installation or it can act as an index listing a subset of rules.

The default rule is just an exemple matching on **Dirtyc0w** source code, but you can easily create your own rules or reuse already existing rule packages.
Include your custom rules into /usr/share/leguichet/rules/index.yar, like that :

.. code-block:: shell-session

 include "./custom/custom_rule.yar"

.. admonition:: Help
 :class: tip

 Take a look at https://github.com/Yara-Rules/rules for more details but keep in mind that some rules are not well maintained and may not fit 
 the version of *libyara* installed via apt. You should test every new rules before adding them in production.

USB mode
--------

When the :ref:`installation` is complete, we must declare at least two USB peripherals which will be authorized to interact with the decontamination station.
The first device will be dedicated to untrusted files (input) and the second must be dedicated to get the analyzed files back (output).

.. image:: img/usbmode.jpg

Open a new terminal and type the following:

.. code-block:: shell-session

 $ udevadm monitor --property | grep ID_SERIAL_SHORT

Plug your first USB device in and note the short serial number.
When done, create a new file in /etc/udev/rules.d/90-usb-leguichet.rules and add the following line:

.. code-block:: shell-session

 #/etc/udev/rules.d/90-usb-leguichet.rules
 ACTION=="add", KERNEL=="sd[a-z][0-9]", SUBSYSTEM=="block", ENV{ID_SERIAL_SHORT}=="XXXXX", RUN+="systemctl start leguichet-usb-in@%k.service"

Update the ID_SERIAL_SHORT variable according to the previously noted value.
Repete now this operation for the second USB device.
And add a second line to /etc/udev/rules.d/90-usb-leguichet.rules.

.. code-block:: shell-session

 #/etc/udev/rules.d/90-usb-leguichet.rules
 ACTION=="add", KERNEL=="sd[a-z][0-9]", SUBSYSTEM=="block", ENV{PRODUCT}=="XXXXX", RUN+="systemctl start leguichet-usb-in@%k.service"
 ACTION=="add", KERNEL=="sd[a-z][0-9]", SUBSYSTEM=="block", ENV{PRODUCT}=="YYYYY", RUN+="systemctl start leguichet-usb-out@%k.service"

Double check that both ENV{PRODUCT} variables are correct.
More devices may of course be added using the same way.

Reload the udev rules:

.. code-block:: shell-session

 udevadm control --reload-rules

.. note::
 You might want to edit the following scripts to adjust the **FORMAT** and **FSTYPE** parameters according to your needs.
 
 .. code-block:: shell-session

    /usr/bin/usb-in
    /usr/bin/usb-out

**FORMAT**: Set the value to *true* of *false* if you want to format the second (output) USB device each time it is plugged in.

**FSTYPE**: Select the fstype desired when formatting (ext2, ext3, ext4, vfat, exfat).

.. warning::
 Connect only one USB device at a time.

We should be ready to go.

Log files are available under:

.. code-block:: shell-session

  /var/log/leguichet-usb/usb-in.log
  /var/log/leguichet-usb/usb-out.log

Watch them using the following command:

.. code-block:: shell-session

 watch -t -n1 "cat usb-in.log"
 watch -t -n1 "cat usb-out.log"
