.. Le-Guichet documentation master file, created by
   sphinx-quickstart on Wed Dec 30 08:13:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. image:: /img/logo.png

User documentation
------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   installation
   administration
   usage
   download
   rasp


