***********************************
Station blanche pour Raspberry Pi 4 
***********************************

**#StationBlancheForAll!**

Ceci est une version 100% opensource de station blanche pour **Raspberry Pi v4**. 

L'image mise à disposition est basée sur le logiciel Le-Guichet et propose une analyse antiviral ainsi qu'une analyse basée 
sur environ 600 règles Yara déjà pré-installées dans l'image. Vous pourrez bien entendu ajouter de nouvelles règles Yara à 
votre convenance afin d'augmenter la probabilité de détection. Vous pourrez également filtrer les types et la taille maximale 
des fichiers à transférer en sortie. Le code est intégralement écrit en Rust, sandboxé et repose sur le principe du moindre privilège.

.. note::
 Malgré tout le soin apporté à la solution, elle peut toutefois encore présenter certains bugs à l'utilisation. N'hésitez pas à nous contacter pour remonter ces bugs qui seront
 corrigés dans les prochaines versions.

.. admonition:: Ce dont la station blanche ne vous protège pas
  :class: warning

  De part la nature et le faible coût de la plateforme Raspberry Pi, la station blanche ne sera évidemment pas protégée contre USB-Killer. Des mécanismes de protection sont déployés contre les attaques de type BadUSB, la station y reste cependant potentiellement vulnérable, sous certaines conditions, pendant la phase de démarrage. Il est fortement conseillé dans ce cas de maitriser le parc des périphériques USB de sortie (comme nous le ferons plus tard dans cette documentation) mais aussi, dans une certaine mesure, celui des périphériques d’entrée.

Téléchargement
==============
- `leguichet-sd-v1.0 <https://le-guichet.com/download/rasp/leguichet-sd-v1.0.tar.gz>`_ (`sha256 <https://le-guichet.com/download/rasp/leguichet-sd-v1.0.tar.gz.sha256>`_)

L'image que vous avez téléchargé se redimensionnera automatiquement en fonction de la taille de votre carte MicroSD. 
Pour copier l'image de la station blanche sur votre carte SD:

.. code-block:: shell-session

 tar -xvzf leguichet-sd-xxyy.tar.gz
 sudo dd if=raspberry_leguichet_system_image.img of=/dev/sdX bs=1M status=progress


Il est cependant conseillé d'utiliser le logiciel bmaptool comme suit:


.. code-block:: shell-session

 tar -xvzf leguichet-sd-xxyy.tar.gz
 sudo bmaptool copy --bmap raspberry_leguichet_system_image.img.bmap raspberry_leguichet_system_image.img /dev/sdX 
  
Où sdX sera le périphérique détecté par votre ordinateur pour la carte SD.

.. note::
 Etant donné le nombre de lecture/écriture élevé sur la carte MicroSD, il est fortement conseillé d'utiliser des cartes haute performance.

Avant de commencer, il faut signer au moins un périphérique USB de sortie (qui receptionnera les fichiers considérés comme sains). 
Tous les périphériques de stockage USB non signés seront considérés comme périphériques d'entrée par défaut (donc contenant des fichiers
potentiellement malveillants).

.. danger::
 Les périphériques USB de sortie que vous allez devoir signer, seront complétement détruits (MBR/Partitions etc...) et reconstruits selon un modèle précis.
 Il n'y a pas de support des tables de partitions au format GPT pour le moment.

Signer un périphérique USB de sortie
====================================

Connexion à la station blanche
------------------------------

L'image fournie est basée sur une distribution GNU/Linux Debian 11 (Bullseye) toujours en cours de durcissement. Le DHCP est activé par défaut: Référez-vous à votre équipement réseau pour obtenir l'adresse IP obtenue par la station blanche après son démarrage.

Afin de prémunir la station blanche d'attaques de type BadUSB, seuls les périphériques USB de type "stockage de masse" comme les clés ou disques durs USB sont reconnus par la station blanche.
Pour signer un périphérique USB de sortie, il faut donc absolument se connecter via SSH sur la station blanche:

.. code-block:: shell-session

 ssh leguichet-sign@192.168.XX.YY (IP obtenue via DHCP)

.. warning:: 
 Le mot de passe par defaut est Changeme. Il conviendra de modifier ce dernier dès la première utilisation en le remplacant par un mot de passe robuste.


Génération des clés de signature
--------------------------------

On va générer maintenant une paire de clés asymétriques qui servira à signer et vérifier les périphériques sortants:

.. code-block:: shell-session

 sudo /usr/bin/leguichet-sign --generate=true --password=Toto007
 sudo chmod 600 /etc/leguichet/leguichet.priv
 sudo chattr +i /etc/leguichet/leguichet.priv

.. warning::

 Il est très important de remplacer le mot de passe dans la ligne de commande par le votre :)

.. danger::
 Cette bi-clé ne doit être générée qu'une seule fois à l'initialisation de la station blanche. Le remplacement de cette bi-clé
 entrainera l'échec de la vérification de la signature de toutes les périphériques USB déjà signés. Par défaut, les clés privées
 et publiques sont enregistrées dans /etc/leguichet/. Il est important de sauvegarder ces clés dans un endroit sécurisé.

Signature d'un périphérique USB
-------------------------------

Une fois la paire de clés correctement générée, éxecutez la commande suivante:

.. code-block:: shell-session

 sudo /usr/bin/leguichet-sign --watch=true

Brancher maintenant le périphérique usb de sortie à signer sur la station blanche. Ce périphérique devra être vide de tout fichier afin d'éviter des transferts non désirés.

Pressez Ctrl+C et copier/coller la ligne qui apparait dans le terminal en la modifiant avec le mot de passe que 
vous avez choisi pour générer la paire de clés précédemment. Par exemple:

.. code-block:: shell-session

 sudo /usr/bin/leguichet-sign -device=/dev/sda --sign=true --password=Toto007 --vendorid=0951 --modelid=160b --revision=1.00 --serial=Kingston_DataTraveler_2.0_0019E000B4625C8B0A070016-0:0

Le nouveau périphérique USB devrait être maintenant correctement signé et formaté en fat32. Vous pouvez bien entendu reformater le périphérique avec tout autre système de fichier supporté par la station blanche (ext2, ext3, ext4, fat32, exfat, ntfs)

.. note::
 Répetez cette procédure avec l'ensemble des périphériques USB que vous souhaitez utiliser en tant que périphériques de sortie.


Une fois l'opération terminée, débranchez le(s) périphérique's) et rebranchez-le(s) afin de s'assurer qu'il(s) est(sont) bien reconnu(s) comme périphérique(s) de sortie.


Utilisation
===========


#. Dans le menu en haut à droite, appuyer sur "Start" ;
#. Brancher d'abord un périphérique d'entrée (Tout périphérique USB non signé devrait être reconnue comme périphérique d'entrée) ;
#. En suivant les instructions à l'écran et une fois les fichiers commençant à apparaitrent dans le guichet de sortie, débrancher le périphérique d'entrée ;
#. Brancher le périphérique de sortie signé avec le périphérique d'entrée debranché ;
#. Plusieurs périphériques d'entrée peuvent être utilisés à la suite avant de brancher le périphérique de sortie ;
#. Tous les fichiers de configuration se situent dans /etc/leguichet/leguichet-\*.conf. Il est notamment possible de contrôler une liste blanche de types de fichers (magic numbers) ainsi que la taille maximale des fichiers à transférer. Veuillez-vous référer à la documentation officielle du Guichet pour plus d'information sur les différentes options (https://le-guichet.com/administration.html#leguichet-in).

.. Warning::
 Par souci de simplicité et de sécurité, les répertoires éventuellement présents sur un périphérique d'entrée ne seront jamais transférés vers la station blanche. Il est donc important de placer les fichiers à transférer directement à la racine du support USB.

Durcissement de la station blanche
==================================

L'image système prête à l'emploi pour Raspberry Pi 4 dispose des fonctionnalités de durcissement suivantes:

- Des protections contre BadUSB (l'écran marche uniquement avec le bus DSI);
- Un firewall NFTables (seul le port SSH est exposé) ;
- Une protection contre le bruteforce SSH ;
- Une protection anti-rebond SSH (pivot SSH) ;
- Un paramètrage spécifique du noyau Linux ;
- Montage des périphériques non signés en RO, NODEV, NOSUID, NOEXEC, NODEV ;
- Kiosk utilisateur sandboxé;
- Démons "Le-Guichet" sandoxés une voir plusieurs fois.

Tous les fichiers transférés dans la station blanche sont automatiquement renommé avec un horodatage. 
Pour chaque fichier transféré, vous pourrez éventuellement trouver, en fonction des résultats des différents scans, les extensions suivantes:

- .sha256: Contient le sha256 digest du fichier transféré;
- .antivirus: le fichier a été détecté par l'antivirus comme malveillant. Le fichier original n'est donc plus disponible;
- .forbidden: l'extension et le magic number ne correspondent pas ou est interdit par l'administrateur;
- .yara: Le moteur Yara a détecté un fichier potentiellement malveillant. Le fichier peut être transféré ou non suivant la configuration de l'administrateur. Par défaut, le fichier est supprimé;
- .tooBig: La taille du fichier est supérieure à celle fixée par l'administrateur. Le fichier n'est pas transféré;
- .failed: Un fichier n'a pas été tranféré complétement (erreur d'entrée/sortie lors de l'arrachage prématuré d'une clé par exemple).



Mises à jour
============

La station blanche installe automatique les dernières signatures antivirales et les mises à jour de sécurité du système lorque celle-ci peut accéder à internet.
Si la station blanche ne peut accéder à internet, il est tout à fait possible d'effectuer les mises à jours via un dépôt local au système d'information.
Les démons "Le-Guichet" ne sont, quant à eux, pas automatiquement mis à jour et nécessitent pour le moment l'installation des nouvelles images qui seront misent à disposition.
Il conviendra donc de faire une sauvegarde des configurations et des clés générées.

Matériel nécessaire
===================

`L'écran officiel. <https://www.raspberrypi.com/products/raspberry-pi-touch-display/>`_

`Le Raspberry Pi 4 8Go de RAM / modèle B. <https://www.raspberrypi.com/products/raspberry-pi-4-model-b/?variant=raspberry-pi-4-model-b-8gb>`_

`L'alimentation. <https://www.raspberrypi.com/products/type-c-power-supply/>`_

.. note:: 
  Aucune donnée n'est et ne sera jamais collectée lors de votre utilisation de la station blanche.

