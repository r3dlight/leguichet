#!/usr/bin/bash
# SPDX-License-Identifier: GPL-3.0-only
# (C) Copyright 2019-2022 Stephane Neveu
# (C) Copyright 2019-2022 Nicolas Bouchinet

set -o nounset -o errexit -o pipefail

# Path to log file
LOG_DIR="/var/log/leguichet-usb"
readonly LOG_DIR

LOG_FILE="${LOG_DIR}/usb-out.log"
readonly LOG_FILE

function log() {
	if [[ ${1:-} ]]; then
		echo "-> ${1}" >> "${LOG_FILE}"
	fi
}

function die() {
	if [[ ${1:-} ]]; then
		echo "Error: ${1:-}" >> "${LOG_FILE}"
		exit 1
	fi
}

# Deletes the temp directories
function cleanup {
	umount "${MNT_DIR}" || true
	rm -rf "${MNT_DIR}" || true
	rm /var/lock/leguichet/leguichet-out || die "Cannot remove lock for leguichet-out."
	log "*** END TASK ***"
	sleep 5

	cat <<-EOF > "${LOG_FILE}"
		*************************************
		*            LE-GUICHET             *
		*************************************
		-> GUICHET-OUT AWAITING NEW TASK.
	EOF
}

function prepare() {
	# Pay attention, the size of /tmp must large enough for your needs !
	# Also double check that /tmp is mounted with "noexec,nodev,nosuid" attribute.
	local TEMP_DIR
	TEMP_DIR="/tmp"
	readonly TEMP_DIR

	if [[ ! -d "${LOG_DIR:-}" ]]; then
		mkdir "${LOG_DIR}"
		touch "${LOG_FILE}"
		
	fi

	# Register the cleanup function to be called on the EXIT signal
	trap cleanup EXIT

	# This creates a new log file.
	cat <<-EOF > "${LOG_FILE}"
		*************************************
		*            LE-GUICHET             *
		*************************************
		-> GUICHET-OUT: NEW TASK DETECTED
	EOF

	log "Second USB device detected: ${DEVICE_NAME}"
	log "Guichet-out path is: ${GUICHET_OUT}"
	log "Selected temporary directory used for transfering files is: ${TEMP_DIR}"

	# The temp directories used, within ${TEMP_DIR}
	MNT_DIR=$(mktemp -d -p "${TEMP_DIR}") || die "Cannot create temporary mount directory in ${TEMP_DIR}."
	readonly MNT_DIR

	log "Temporary mountpoint is: ${MNT_DIR}."

	# check if tmp dirs are created
	if [[ ! -d "${MNT_DIR:-}" ]]; then
		die "Cannot not create temporary outgoing mount point: ${MNT_DIR}"
	fi
}

function format_out_key() {
		# Choose to format the second key or not.
		local FORMAT
		FORMAT="true"
		readonly FORMAT

		# Choose the filesystem to format with (ext3, ext4, vfat, exfat)
		local FSTYPE
		FSTYPE="vfat"
		readonly FSTYPE

		if [[ "${FORMAT:-}" == true ]]; then
			log "Formatting USB device: ${DEVICE_NAME}..."
			log "Filesystem selected is: ${FSTYPE}."
			if [[ -x /usr/sbin/mkfs.${FSTYPE:-} ]]; then
				mkfs."${FSTYPE}" "/dev/${DEVICE_NAME}" >> "${LOG_FILE}" || die "Cannot format USB device: ${DEVICE_NAME}."
			else
				die "Cannot find mkfs.${FSTYPE} in path."
			fi
		fi
}

function main() {
	# The device name as shown using the dmesg command.
	local DEVICE_NAME
	DEVICE_NAME=${1}
	readonly DEVICE_NAME

	# This path should be ok by default.
	# If Leguichet is installed by default,
	# there is no need to modify basically.
	local GUICHET_OUT
	GUICHET_OUT="/var/local/out/"
	readonly GUICHET_OUT
	
	if [[ -f /var/lock/leguichet/leguichet-transit ]]; then
		rm /var/lock/leguichet/leguichet-transit
	fi

	if [[ ! -d /var/lock/leguichet ]]; then
		install -d -m 0750 -o root -g root /var/lock/leguichet
	fi

	touch /var/lock/leguichet/leguichet-out || die "Cannot create lock for leguichet-out."
	prepare
	format_out_key

	# Mount the usb device read only.
	mount -o rw,noexec,nosuid,nodev "/dev/${DEVICE_NAME}" "${MNT_DIR}" || die "Cannot mount outgoing mount point."
	log "USB device mounted with options: rw,noexec,nosuid,nodev."

	# Let's start copying on usb
	sleep 3
	cp "${GUICHET_OUT}"/* "${MNT_DIR}/" || die "No files found to copy on the device."
	log "-> Files are successfully copied."
	sleep 1

	# Clean up the GUICHET_OUT directory.
	rm "${GUICHET_OUT}"/* || die "Cannot cleanup ${GUICHET_OUT} directory."
	log "All files cleared in ${GUICHET_OUT}."

	# Sync disks.
	sync || die "While syncing disks."
	log "Syncing done."

	# Unmounting the tmp directory.
	umount "${MNT_DIR}" || die "Cannot unmount outgoing USB disk."
	log "Mountpoint: ${MNT_DIR} is unmounted."

	# Play a bip when all done.
	sleep 2
	log "Files are now ready on the second USB device."
	log "You can unplug it and enjoy :-)"
}

main "$@"
